# Jacarta-Tool

Утилита для использования аппаратных токенов Jacarta-2 ГОСТ.

## Инициализация токена

Процесс инициализации предполагает удаление всех объектов, за исключением информации о датчике случайных чисел. При этом:

- значение PIN-кода администратора остаётся прежним,
- значение PIN-кода пользователя удаляется.

    jacarta-tool --init-token --pin <ADMIN_PIN> --user-pin <USER_PIN>

где:

    - ADMIN_PIN - PIN-код администратора;
    - USER_PIN - PIN-код пользователя.

## Генерация ключевой пары

Генерация ключевой пары в соответствии со стандартом ГОСТ 34.10-2001:

    jacarta-tool --create-keys -id <ID> --label <LABEL> --login --pin <PIN>

где:

    - ID - дескриптор закрытого ключа (положительное целое число);
    - LABEL - метка для создаваемого открытого ключа;
    - PIN - PIN-код пользователя;

Например:

    jacarta-tool --create-keys --id 1 --label "mk1" --login --pin 1234567890

## Генерация запроса на подпись сертификата

    jacarta-tool --create-csr -o <OUTPUT_FILE> --login --pin <PIN> \
                 --dn "key1=value1" [--dn "key2=value2" ...]
                 --ext "key1=value1" [--ext "key2=value2" ...]
                 --attr "key1=value1" [--attr "key2=value2" ...]
где:

    - OUTPUT_FILE - файл запроса на подпись в кодировке DER;
    - PIN - PIN-код пользователя;
    - dn - части отличительного имени (distinguished name);
    - ext - части расширений;
    - attr - части дополнительных атрибутов.

    dn, ext и attr задаются парами строк, первая из которых должна
    содержать тип или идентификатор поля (OID), вторая - значение в
    кодировке UTF-8.

Пример:

    jacarta-tool \
        --create-csr -o csr.der --login --pin 1234567890 \
        --dn "street=Бейкер стрит 221б" --dn "CN=Шерлок Холмс" --dn "C=UK" \
        --ext "keyUsage=digitalSignature,nonRepudiation,keyEncipherment,dataEncipherment,keyAgreement" \
        --ext "extendedKeyUsage=critical,clientAuth,emailProtection,1.2.643.2.2.34.6,1.2.643.3.7.0.1.12,1.2.643.3.7.8.1" \
        --ext "1.2.643.100.111=ASN1:UTF8String:jacarta_ecp"

## Запись сертификата на токен

    jacarta-tool --store-certificate --label <LABEL> -f <INPUT_FILE> --login --pin <PIN>

где:

    - label - метка для объекта сертификата на токене;
    - INPUT_FILE - файл сертификата в кодировке DER;
    - PIN - PIN-код пользователя;

Пример:

    jacarta-tool --store-certificate --label "My certificate" -f certificate.der --login --pin 1234567890

## Чтение сертификатов с токена

    jacarta-tool --list-certs

## Чтение открытых ключей

    jacarta-tool --list-public-keys

## Чтение закрытых ключей

    jacarta-tool --list-private-keys --login --pin 1234567890

## Удаление сертификата с токена

    jacarta-tool --delete-cert <NUMBER>

где NUMBER - номер сертификата (положительное целое число, которое можно
узнать, читая список сертификатов).

## Дополнительные опции

- `--module <PATH/TO/libjcpkcs11-2.so>`

    Задает путь до библиотекции, реализующей PKSC #11, от компании Аладдин.
    По умолчанию равен `/usr/lib64/pkcs11/libjcpkcs11-2.so`.
