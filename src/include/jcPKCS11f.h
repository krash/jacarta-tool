#include "jcPKCS11t.h"

/**
* Получить список функций-расширений
* @param ppFunctionList список функций-расширений
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_GetFunctionList)
#ifdef CK_NEED_ARG_LIST
(
    JC_FUNCTION_LIST_PTR_PTR ppFunctionList
);
#endif

/**
* Получить информацию о считывателе.
*
* @param pReaderName имя считывателя
* @param ulReaderNameSize размер имени считывателя в байтах. М.б. равен CK_UNAVAILABLE_INFORMATION если имя считывателя заканчивается 0
* @param pProperties информация о считывателе
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_GetReaderProperties)
#ifdef CK_NEED_ARG_LIST
(
    CK_UTF8CHAR_PTR pReaderName,
    CK_ULONG ulReaderNameSize,
    JC_TOKEN_PROPERTIES_PTR pProperties
);
#endif

/**
* Установить метку. Требуется аутентификация.
* @param slotID идентификатор слота
* @param pLabel метка
* @param ulLabelSize размер метки в байтах (не более 32. М.б. равен CK_UNAVAILABLE_INFORMATION если метка заканчивается 0)
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_SetLabel)
#ifdef CK_NEED_ARG_LIST
(
    CK_SLOT_ID slotID,
    CK_UTF8CHAR_PTR pLabel,
    CK_ULONG ulLabelSize
);
#endif

/**
* Получить информацию о ридере
* @param slotID идентификатор слота
* @param pISD информация о ридере
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_GetISD)
#ifdef CK_NEED_ARG_LIST
(
    CK_SLOT_ID slotID,
    JC_ISD_DATA_PTR pISD
);
#endif

/**
* Установить точку монтирования для Jacarta MicroSD
* @param pMountPoint абсолютный путь до точки монтирования Jacarta MicroSD
* @param ulMountPointSize длина пути в байтах. М.б. CK_UNAVAILABLE_INFORMATION, если путь заканчивается 0
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_SD_SetMountPoint)
#ifdef CK_NEED_ARG_LIST
(
    CK_UTF8CHAR_PTR pMountPoint,
    CK_ULONG ulMountPointSize
);
#endif

/**
* Получить точку монтирования для Jacarta MicroSD
* @param pMountPoint буфер для абсолютного пути до точки монтирования Jacarta MicroSD
* @param pulMountPointSize длина пути в байтах
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_SD_GetMountPoint)
#ifdef CK_NEED_ARG_LIST
(
    CK_UTF8CHAR_PTR pMountPoint,
    CK_ULONG_PTR pulMountPointSize
);
#endif

/**
* Установка параметров персонализации для PKI апплета. Параметры персонализации применяются только при инициализации апплета
* @param slotID идентификатор слота
* @param pInfo параметры персонализации
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_PKI_SetComplexity)
#ifdef CK_NEED_ARG_LIST
(
    CK_SLOT_ID slotID,
    JC_PKI_PERSONALIZATION_INFO_PTR pInfo
);
#endif

/**
* Получение параметров персонализации для PKI апплета.
* @param slotID идентификатор слота
* @param pInfo параметры персонализации
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_PKI_GetComplexity)
#ifdef CK_NEED_ARG_LIST
(
    CK_SLOT_ID slotID,
    JC_PKI_PERSONALIZATION_INFO_PTR pInfo
);
#endif

/**
* Очистка содержимого карты Laser. Метод требует аутентификации администратором.
* @param slotID идентификатор слота
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_PKI_WipeCard)
#ifdef CK_NEED_ARG_LIST
(
    CK_SLOT_ID slotID
);
#endif

/**
* Получить счетчики ПИН-кодов для Laser.
* @param slotID идентификатор слота
* @param pInfo счетчики ПИН-кодов
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_PKI_GetPINInfo)
#ifdef CK_NEED_ARG_LIST
(
    CK_SLOT_ID slotID,
    JC_PKI_PIN_INFO_PTR pInfo
);
#endif

/**
* Получить challenge для внешней аутентификации Laser.
* @param slotID идентификатор слота
* @param pChallange буфер для challenge
* @param ulChallangeSize размер буфера в байтах
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_PKI_GetChallenge)
#ifdef CK_NEED_ARG_LIST
(
    CK_SLOT_ID slotID,
    CK_BYTE_PTR pChallange,
    CK_ULONG ulChallangeSize
);
#endif

/**
* Разблокировать пин пользователя для Laser. требует аутентификации администратором.
* @param slotID идентификатор слота
* @return   код ошибки
*           CKR_CANNOT_UNLOCK если разблокировка не возможна или код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_PKI_UnlockUserPIN)
#ifdef CK_NEED_ARG_LIST
(
    CK_SLOT_ID slotID
);
#endif

/**
* Получить информацию о поддержке биометрии
* @param slotID идентификатор слота
* @param pInfo информация о поддержке биометрии
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_PKI_BIO_GetSupported)
#ifdef CK_NEED_ARG_LIST
(
    CK_SLOT_ID slotID,
    JC_PKI_BIO_SUPPORT_INFO_PTR pInfo
);
#endif

/**
* Получить идентификаторы зарегистрированных пальцев
* @param slotID идентификатор слота
* @param pFingers буфер для идентификаторов пальцев
* @param pulFingerCount количество идентификаторов пальцев
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_PKI_BIO_GetFingerIndexes)
#ifdef CK_NEED_ARG_LIST
(
    CK_SLOT_ID slotID,
    CK_BYTE_PTR pFingers,
    CK_ULONG_PTR pulFingerCount
);
#endif

/**
* Получить публичную биометрическую информацию о пальце по его идентификатору.
* @param slotID идентификатор слота
* @param fingerIndex идентификатор пальца (от 1 до 10)
* @param pPublicData буфер для публичной биометрической информации о пальце
* @param pulPublicDataSize размер буфера в байтах
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_PKI_BIO_GetFingerPublicData)
#ifdef CK_NEED_ARG_LIST
(
    CK_SLOT_ID slotID,
    CK_BYTE fingerIndex,
    CK_BYTE_PTR pPublicData,
    CK_ULONG_PTR pulPublicDataSize
);
#endif

/**
* Установить биометрическую информацию о пальце по его идентификатору. Требует аутентификации администратором.
* @param slotID идентификатор слота
* @param fingerIndex идентификатор пальца (от 1 до 10)
* @param pPublicData буфер публичной биометрической информации о пальце
* @param ulPublicDataSize размер буфера публичной информации в байтах
* @param pPrivateData буфер закрытой биометрической информации о пальце
* @param ulPrivateDataSize размер буфера закрытой информации в байтах
* @param pDeviceName имя устройства
* @param ulDeviceNameSize размер имени устройства в байтах
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_PKI_BIO_SetFingerData)
#ifdef CK_NEED_ARG_LIST
(
    CK_SLOT_ID slotID,
    CK_BYTE fingerIndex,
    CK_BYTE_PTR pPublicData,
    CK_ULONG ulPublicDataSize,
    CK_BYTE_PTR pPrivateData,
    CK_ULONG ulPrivateDataSize,
    CK_BYTE_PTR pDeviceName,
    CK_ULONG ulDeviceNameSize
);
#endif

/**
* Удалить отпечаток по идентификатору пальца. Требует аутентификации администратором.
* @param slotID идентификатор слота
* @param fingerIndex идентификатор пальца (от 1 до 10)
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_PKI_BIO_DeleteFinger)
#ifdef CK_NEED_ARG_LIST
(
    CK_SLOT_ID slotID,
    CK_BYTE fingerIndex
);
#endif

/**
* Установить путь к биометрической библиотеке
* @param pLibraryPath абсолютный путь до библиотеки
* @param ulLibraryPathSize размер пути до библиотеки в байтах или CK_UNAVAILABLE_INFORMATION, если путь заканчивается 0.
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_PKI_BIO_SetLibrary)
#ifdef CK_NEED_ARG_LIST
(
    CK_UTF8CHAR_PTR pLibraryPath,
    CK_ULONG ulLibraryPathSize
);
#endif

/**
* Инициализировать генератор псевдослучайных чисел в апплете Криптотокен-1. Требует аутентификации пользователем.
* @param slotID идентификатор слота
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_CT1_InitPrng)
#ifdef CK_NEED_ARG_LIST
(
    CK_SLOT_ID slotID
);
#endif

/**
* Выполнить внутренние тесты в апплете Криптотокен-1. Требует аутентификации пользователем.
* @param slotID идентификатор слота
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_CT1_DoTests)
#ifdef CK_NEED_ARG_LIST
(
    CK_SLOT_ID slotID
);
#endif

/**
* Проверить персонализирован ли Криптотокен-2.
* @param slotID идентификатор слота
* @param pPersonalized признак персонализации
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_CT2_IsPersonalized)
#ifdef CK_NEED_ARG_LIST
(
    CK_SLOT_ID slotID,
    CK_BBOOL_PTR pPersonalized
);
#endif

/**
* Получить дополнительную информацию о Криптотокен-2.
* @param slotID идентификатор слота
* @param pInfo дополнительная информацию о Криптотокен-2
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_CT2_ReadExtInfo)
#ifdef CK_NEED_ARG_LIST
(
    CK_SLOT_ID slotID,
    JC_CT2_EXTENDED_INFO_PTR pInfo
);
#endif

/**
* Рассчитать контрольную сумму Криптотокен-2.
* @param slotID идентификатор слота
* @param pCheckSum буфер для контрольной суммы
* @param pulCheckSumSize длина контрольной суммы в байтах
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_CT2_CalcCheckSum)
#ifdef CK_NEED_ARG_LIST
(
    CK_SLOT_ID slotID,
    CK_BYTE_PTR pCheckSum,
    CK_ULONG_PTR pulCheckSumSize
);
#endif

/**
* Установить ПИН-код подписи для КТ2. Требуется аутентификация пользователем
* @param slotID идентификатор слота
* @param pPin ПИН-код подписи
* @param ulPinSize длина ПИН-кода подписи в байтах. М.б. CK_UNAVAILABLE_INFORMATION если ПИН-код подписи заканчивается 0
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_CT2_SetSignaturePIN)
#ifdef CK_NEED_ARG_LIST
(
    CK_SLOT_ID slotID,
    CK_UTF8CHAR_PTR pPin,
    CK_ULONG ulPinSize
);
#endif

/**
* Установить ПИН-код подписи для КТ2. Требуется аутентификация пользователем.
* @param slotID идентификатор слота
* @param pOldPin старый ПИН-код подписи
* @param ulOldPinSize длина старого ПИН-кода подписи в байтах. М.б. CK_UNAVAILABLE_INFORMATION если ПИН-код подписи заканчивается 0
* @param pNewPin новый ПИН-код подписи
* @param ulNewPinSize длина нового ПИН-кода подписи в байтах. М.б. CK_UNAVAILABLE_INFORMATION если ПИН-код подписи заканчивается 0
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_CT2_ChangeSignaturePIN)
#ifdef CK_NEED_ARG_LIST
(
    CK_SLOT_ID slotID,
    CK_UTF8CHAR_PTR pOldPin,
    CK_ULONG ulOldPinSize,
    CK_UTF8CHAR_PTR pNewPin,
    CK_ULONG ulNewPinSize
);
#endif

/**
* Установить ПУК-код для КТ2. Требуется аутентификация администратором.
* @param slotID идентификатор слота
* @param pPin ПУК-код
* @param ulPinSize длина ПУК-кода в байтах. М.б. CK_UNAVAILABLE_INFORMATION если ПУК-код заканчивается 0
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_CT2_SetPUK)
#ifdef CK_NEED_ARG_LIST
(
    CK_SLOT_ID slotID,
    CK_UTF8CHAR_PTR pPuk,
    CK_ULONG ulPukSize
);
#endif

/**
* Установить политику ПИН-кода. Требуется аутентификация администратором.
* @param slotID идентификатор слота
* @param pinType тип ПИН-кода
* @param pPinPolicy политика ПИН-кода
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_CT2_SetPINPolicy)
#ifdef CK_NEED_ARG_LIST
(
    CK_SLOT_ID slotID,
    JC_CT2_PIN_TYPE pinType,
    JC_CT2_PIN_POLICY_PTR pPinPolicy
);
#endif

/**
* Управление секретным каналом
* @param hSession дескриптор сессии
* @param bEnabled CK_TRUE для разрешения секретного канала, CK_FALSE - для запрещения
* @param bExclusive CK_TRUE, если секретный канал требуется в эксклюзивном режиме
* @param hPublicKey дескриптор открытого ключа, если секретный канал д.б. установлен на ключевых парах. Иначе - CK_INVALID_HANDLE
* @param pPrivateKeyValue буфер значения закрытого ключа, если секретный канал д.б. установлен на ключевых парах. Иначе - NULL
* @param ulPrivateKeyValueSize размер буфера значения закрытого ключа в байтах, если секретный канал д.б. установлен на ключевых парах. Иначе - 0
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_CT2_SetSecureMessaging)
#ifdef CK_NEED_ARG_LIST
(
    CK_SESSION_HANDLE hSession,
    CK_BBOOL bEnabled,
    CK_BBOOL bExclusive,
    CK_OBJECT_HANDLE hPublicKey,
    CK_BYTE_PTR pPrivateKeyValue,
    CK_ULONG ulPrivateKeyValueSize
);
#endif

/**
* Получить версию Атнифрод-считывателя
* @param    slotID                  идентификатор слота
* @param    pulOSVersion            версия ОС
* @param    pulApplicationVersion   версия приложения
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_AFT_GetReaderVersion)
#ifdef CK_NEED_ARG_LIST
(
    CK_SLOT_ID slotID,
    CK_ULONG_PTR pulOSVersion,
    CK_ULONG_PTR pulApplicationVersion
);
#endif

/**
* Получить работает ли Атнифрод-считыватель в бескарточном режиме
* @param    slotID      идентификатор слота
* @param    pSupported  признак поддержки бес карточного режима. Равен CK_TRUE, если бескарточный режим поддерживается
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_AFT_IsCardlessSupported)
#ifdef CK_NEED_ARG_LIST
(
    CK_SLOT_ID slotID,
    CK_BBOOL_PTR pSupported
);
#endif

/**
* Включить SWYX-режим.
* @param    slotID      идентификатор слота
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_SWYX_Start)
#ifdef CK_NEED_ARG_LIST
(
    CK_SLOT_ID slotID
);
#endif


/**
* Отобразить текст на экране SWYX-считывателя и предложить пользователю подписать его.
* В случае если пользователь отменит операцию подписи, метод JC_SWYX_Display вернет CKR_FUNCTION_REJECTED (0x00000200),
в случае тайм-аута - CKR_FUNCTION_CANCELED (0x00000050)
* @param    slotID          идентификатор слота
* @param    language        Код языка. 0x0409 - английский, 0x0419 - русский.
* @param    ulTimeout       тайм-аут
* @param    ulDisplayIndex  индекс дисплея
* @param    pText           текст для отображения на экране считывателя в кодировке UTF8 длиной от 5 до 400 символов
* @param    ulTextSize      длина текста в байтах
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_SWYX_Display)
#ifdef CK_NEED_ARG_LIST
(
    CK_SLOT_ID slotID,
    CK_ULONG language,
    CK_ULONG ulTimeout,
    CK_ULONG ulDisplayIndex,
    CK_UTF8CHAR_PTR pText,
    CK_ULONG ulTextSize
);
#endif

/**
* Выключить SWYX-режим.
* @param    slotID              идентификатор слота
* @param    pSignature          буфер для подписи
* @param    pulSignatureSize    размер буфера для подписи в байтах
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_SWYX_Stop)
#ifdef CK_NEED_ARG_LIST
(
    CK_SLOT_ID slotID,
    CK_BYTE_PTR pSignature,
    CK_ULONG_PTR pulSignatureSize
);
#endif

/**
* Инициализировать карту, вставленную в Антифрод-считыватель (Криптотокен)
* @param    slotID          идентификатор слота
* @param    ulLanguageId    Код языка. 0x0409 - английский, 0x0419 - русский.
* @param    ulTimeout       тайм-аут
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_AFT_InitCard)
#ifdef CK_NEED_ARG_LIST
(
    CK_SLOT_ID slotID,
    CK_ULONG ulLanguageId,
    CK_ULONG ulTimeout
);
#endif


/**
* Получить серийный номер апплета, находящегося внутри Антифрод-считывателя.
* @param    slotID          идентификатор слота
* @param    pSeraul         буфер для серийного номера
* @param    pulSerialSize   размер буфера для серийного номера в байтах
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_AFT_GetSerialNumber)
#ifdef CK_NEED_ARG_LIST
(
    CK_SLOT_ID slotID,
    CK_BYTE_PTR pSerial,
    CK_ULONG_PTR pulSerialSize
);
#endif

/**
* Инициализировать ПИН-код пользователя на карте в Антифрод-считывателе (Криптотокен).
* @param    slotID              идентификатор слота
* @param    ulLanguageId        Код языка. 0x0409 - английский, 0x0419 - русский.
* @param    ulTimeout           тайм-аут
* @param    bConfirmRequired    Требовать ли повторного ввода ПИН-кода для подтверждения. CK_TRUE - требовать
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_AFT_InitUserPin)
#ifdef CK_NEED_ARG_LIST
(
    CK_SLOT_ID slotID,
    CK_ULONG ulLanguageId,
    CK_ULONG ulTimeout,
    CK_BBOOL bConfirmRequired
);
#endif

/**
* Запросить ПИН-код и проверить его на карте.
* @param    slotID              идентификатор слота
* @param    userType            тип ПИН-кода
* @param    ulLanguageId        Код языка. 0x0409 - английский, 0x0419 - русский.
* @param    ulTimeout           тайм-аут
* @return код ошибки или CKR_FUNCTION_REJECTED, если пользователь отменил операцию
*/
CK_PKCS11_FUNCTION_INFO(JC_AFT_VerifyPin)
#ifdef CK_NEED_ARG_LIST
(
    CK_SLOT_ID slotID,
    CK_USER_TYPE userType,
    CK_ULONG ulLanguageId,
    CK_ULONG ulTimeout
);
#endif

/**
* Сменить ПИН-код.
* @param    slotID              идентификатор слота
* @param    userType            тип ПИН-кода
* @param    ulLanguageId        Код языка. 0x0409 - английский, 0x0419 - русский.
* @param    ulTimeout           тайм-аут
* @param    bConfirmRequired    Требовать ли повторного ввода ПИН-кода для подтверждения. CK_TRUE - требовать
* @return код ошибки или CKR_FUNCTION_REJECTED, если пользователь отменил операцию
*/
CK_PKCS11_FUNCTION_INFO(JC_AFT_ModifyPin)
#ifdef CK_NEED_ARG_LIST
(
    CK_SLOT_ID slotID,
    CK_USER_TYPE userType,
    CK_ULONG ulLanguageId,
    CK_ULONG ulTimeout,
    CK_BBOOL bConfirmRequired
);
#endif

/**
* Получить свойства считывателя.
* @param    slotID      идентификатор слота
* @param    pProperties свойства считывателя
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_AFT_GetProperties)
#ifdef CK_NEED_ARG_LIST
(
    CK_SLOT_ID slotID,
    SWYX_PROPERTIES_RESPONSE_PTR pPropeties
);
#endif

/**
* Сгенерировать новую ключевую пару на считывателе. Карта может отсутствовать
* @param    slotID      идентификатор слота
* @return код ошибки или CKR_FUNCTION_REJECTED, если пользователь отменил операцию
*/
CK_PKCS11_FUNCTION_INFO(JC_AFT_Personalize)
#ifdef CK_NEED_ARG_LIST
(
    CK_SLOT_ID slotID
);
#endif

/**
* Получить открытый ключ считывателя. Карта может отсутствовать
* @param    slotID              идентификатор слота
* @param    pPublicKey          открытый ключ
* @param    pulPublicKeySize    размер открытого ключа в байтах
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_AFT_GetPublicKey)
#ifdef CK_NEED_ARG_LIST
(
    CK_SLOT_ID slotID,
    CK_BYTE_PTR pPublicKey,
    CK_ULONG_PTR pulPublicKeySize
);
#endif

/**
* Запросить у пользователя ПИН-код и сохранить его внутри считывателя (но не на карте). Карта может отсутствовать
* @param    slotID              идентификатор слота
* @param    ulLanguageId        Код языка. 0x0409 - английский, 0x0419 - русский.
* @param    ulTimeout           тайм-аут
* @param    bConfirmRequired    Требовать ли повторного ввода ПИН-кода для подтверждения. CK_TRUE - требовать
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_AFT_EnterLocalPin)
#ifdef CK_NEED_ARG_LIST
(
    CK_SLOT_ID slotID,
    CK_ULONG ulLanguageId,
    CK_ULONG ulTimeout,
    CK_BBOOL bConfirmRequired
);
#endif

/**
* Записать ПИН-код переданный приложением внутрь считывателя (но не на карту). Карта может отсутствовать
* @param    slotID          идентификатор слота
* @param    ulLanguageId    Код языка. 0x0409 - английский, 0x0419 - русский.
* @param    ulTimeout       тайм-аут
* @param    pPin            ПИН-код
* @param    ulPinLength     размер ПИН-кода в байтах
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_AFT_WriteLocalPin)
#ifdef CK_NEED_ARG_LIST
(
    CK_SLOT_ID slotID,
    CK_ULONG ulLanguageId,
    CK_ULONG ulTimeout,
    CK_BYTE_PTR pPin,
    CK_ULONG ulPinLength
);
#endif

/**
* Запрашивает ПИН-код и возвращает его значение
* @return CKR_BUFFER_TOO_SMALL если буфер указан (pPin != NULL) и его размер слишком мал для ПИН-кода
*/
/**
* Запросить ПИН-код и вернуть его приложению. Карта может отсутствовать
* @param    slotID          идентификатор слота
* @param    userType        тип пользователя. CK_UNAVAILABLE_INFORMATION - если тип пользователя неизвестен
* @param    ulLanguageId    Код языка. 0x0409 - английский, 0x0419 - русский.
* @param    ulTimeout       тайм-аут
* @param    confirmMode     режим подтверждения ввода
* @param    pPin            режим подтверждения ввода
* @param    ulPinLength     входной - размер буфера для ПИН-кода в байтах, выходной - размер ПИН-кода в байтах
* @return код ошибки или CKR_BUFFER_TOO_SMALL если буфер указан (pPin != NULL) и его размер слишком мал для ПИН-кода
*/
CK_PKCS11_FUNCTION_INFO(JC_AFT_EnterAndReadPin)
#ifdef CK_NEED_ARG_LIST
(
    CK_SLOT_ID slotID,
    CK_USER_TYPE userType,
    CK_ULONG ulLanguageId,
    CK_ULONG ulTimeout,
    JC_CONFIRM_MODE confirmMode,
    CK_BYTE_PTR pPin,
    CK_ULONG_PTR pulPinLength
);
#endif

/**
* Формирование PKCS#7 сообщения типа signed data.
* @param hSession PKCS#11 сессия.
* @param pData данные для подписи.
* @param pDataLength длина данных для подписи.
* @param hSignCertificate сертификат создателя сообщения.
* @param pEnvelope указатель на указатель на буфер в который будет записано сообщение. Буфер создается внутри функции. После окончания работы с ним необходимо освободить его, вызвав функцию freeBuffer().
* @param pulEnvelopeSize указатель на длину созданного буфера с сообщением.
* @param hPrivateKey закрытый ключ создателя сообщения. Может устанавливаться в CK_INVALID_HANDLE, тогда поиск закрытого ключа будет осуществляться по CKA_ID сертификата.
* @param phCertificates указатель на массив сертификатов, которые следует добавить в сообщение.
* @param ulCertificatesCount количество сертификатов в параметре certificates.
* @param flags флаги
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(pkcs7Sign)
#ifdef CK_NEED_ARG_LIST
(
    CK_SESSION_HANDLE hSession,
    CK_BYTE_PTR pData,
    CK_ULONG ulDataLength,
    CK_OBJECT_HANDLE hSignCertificate,
    CK_BYTE_PTR_PTR pEnvelope,
    CK_ULONG_PTR oulEnvelopeSize,
    CK_OBJECT_HANDLE hPrivateKey,
    CK_OBJECT_HANDLE_PTR phCertificates,
    CK_ULONG ulCertificatesCount,
    CK_FLAGS flags
);
#endif

/**
* Проверка подписи в PKCS#7 сообщении типа signed data. Используются программные реализации методов проверки подписи и хеширования.
* @param pEnvelope PKCS#7 сообщение.
* @param pEnvelopeSize длина PKCS#7 сообщения.
* @param pData если сообщение не содержит самих данных, то необходимо передать их в этот параметр.
* @param pDataSize длина данных.
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(pkcs7Verify)
#ifdef CK_NEED_ARG_LIST
(
    CK_BYTE_PTR pEnvelope,
    CK_ULONG ulEnvelopeSize,
    CK_BYTE_PTR pData,
    CK_ULONG ulDataSize
);
#endif

/**
Проверка подписи в PKCS#7 сообщении типа signed data. Используется аппаратная реализация проверки подписи.
* @param hSession PKCS#11 сессия.
* @param pEnvelope PKCS#7 сообщение.
* @param pEnvelopeSize длина PKCS#7 сообщения.
* @param pData Если сообщение не содержит данных (используется отсоединенная подпись), то необходимо передать их в этот параметр.
* @param pDataSize длина данных.
* @param flags флаги. Может принимать значение 0 или PKCS7_HARDWARE_HASH.
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(pkcs7VerifyHW)
#ifdef CK_NEED_ARG_LIST
(
    CK_SESSION_HANDLE hSession,
    CK_BYTE_PTR pEnvelope,
    CK_ULONG pEnvelopeSize,
    CK_BYTE_PTR pData,
    CK_ULONG pDataSize,
    CK_FLAGS flags
);
#endif

/**
* Проверка подписи в PKCS#7 сообщении типа signed data с дополнительной проверкой
* на соответствие подписи ключа проверки ЭП открытому ключу доверенного сертификата. Используется аппаратная реализация проверки подписи.
* @param hSession               PKCS#11 сессия.
* @param pEnvelope          PKCS#7 сообщение.
* @param pEnvelopeSize      Длина PKCS#7 сообщения.
* @param pData              Если сообщение не содержит данных (используется отсоединенная подпись), то необходимо передать их в этот параметр.
* @param pDataSize          Длина данных.
* @param pTrustedSigner     Буфер с доверенным сертификатом в DER формате.
* @param pTrustedSignerSize Длина буфера.
* @param flags Флаги. Может принимать значение 0 или PKCS7_HARDWARE_HASH.
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(pkcs7TrustedVerifyHW)
#ifdef CK_NEED_ARG_LIST
(
    CK_SESSION_HANDLE hSession,
    CK_BYTE_PTR pEnvelope,
    CK_ULONG pEnvelopeSize,
    CK_BYTE_PTR pData,
    CK_ULONG pDataSize,
    CK_BYTE_PTR pTrustedSigner,
    CK_ULONG pTrustedSignerSize,
    CK_FLAGS flags
);
#endif

/**
* @fn checkCertSignature
* Проверка подписи сертификата на соответствие ключу его подписанта.
* Проверка подписи выполняется аппаратно. Хеширование выполняется аппаратно, если
* была вызвана функция useHardwareHash(CK_TRUE).
* @param    hSession                PKCS11# сессия.
* @param    pCertificate            Буфер с сертификатом в DER формате, проверка подписи по которому выполняется.
* @param    ulCertificateSize   Длина буфера.
* @param    pTrustedSignerCertificate           Буфер с сертификатом доверенного подписанта проверяемого сертификата в DER формате.
* @param    ulTrustedSignerCertificateSize  Длина буфера.
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(checkCertSignature)
#ifdef CK_NEED_ARG_LIST
(
    CK_SESSION_HANDLE hSession,
    CK_BYTE_PTR pCertificate,
    CK_ULONG ulCertificateSize,
    CK_BYTE_PTR pTrustedSignerCertificate,
    CK_ULONG ulTrustedSignerCertificateSize
);
#endif

/**
* Извлечение данных и сертификата подписанта из PKCS#7 контейнера.
* @param    pEnvelope       PKCS#7 контейнер.
* @param    ulEnvelopeSize  Длина контейнера.
* @param    pSignerCertificate      Буфер для записи сертификата.
* @param    pulSignerCertificate    Длина буфера.
* @param    pAttachedData           Буфер для записи данных.
* @param    ulAttachedDataSize      Длина буфера.
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(pkcs7Parse)
#ifdef CK_NEED_ARG_LIST
(
    CK_BYTE_PTR pEnvelope,
    CK_ULONG ulEnvelopeSize,
    CK_BYTE_PTR * pSignerCertificate,
    CK_ULONG_PTR pulSignerCertificate,
    CK_BYTE_PTR * pAttachedData,
    CK_ULONG_PTR ulAttachedDataSize
);
#endif

/**
* Извлечение данных, подписи и сертификата подписанта из PKCS#7 контейнера.
* @param    pEnvelope       PKCS#7 контейнер.
* @param    ulEnvelopeSize  Длина контейнера.
* @param    ppSignerCert        Буфер для записи сертификата.
* @param    pulSignerCertLen    Длина буфера.
* @param    ppAttachedData          Буфер для записи данных.
* @param    pulAttachedDataLength       Длина буфера.
* @param    ppSignature       Буфер для записи подписи.
* @param    pulSignatureLength Длина буфера.
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(pkcs7ParseEx)
#ifdef CK_NEED_ARG_LIST
(
    CK_BYTE_PTR pEnvelope,
    CK_ULONG ulEnvelopeSize,
    CK_BYTE_PTR * ppSignerCert,
    CK_ULONG_PTR pulSignerCertLen,
    CK_BYTE_PTR * ppAttachedData,
    CK_ULONG_PTR pulAttachedDataLength,
    CK_BYTE_PTR * ppSignature,
    CK_ULONG_PTR pulSignatureLength
);
#endif


/**
* Проверка пути сертификации.
* @param session PKCS#11 сессия.
* @param certificateToVerify сертификат, который необходимо проверить.
* @param trustedCertificates массив доверенных сертификатов.
* @param trustedCertificatesLength количество сертификатов в trustedCertificates.
* @param certificateChain промежуточные сертификаты.
* @param certificateChainLength количество сертификатов в certificateChain.
* @param crls массив списков отозванных сертификатов.
* @param crlsLengths массив с длинами списков отозванных сертификатов.
* @param crlsLength количество списков отозванных сертификатов в crls.
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(certVerify)
#ifdef CK_NEED_ARG_LIST
(
    CK_SESSION_HANDLE session,
    CK_OBJECT_HANDLE certificateToVerify,
    CK_OBJECT_HANDLE_PTR trustedCertificates,
    CK_ULONG trustedCertificatesLength,
    CK_OBJECT_HANDLE_PTR certificateChain,
    CK_ULONG certificateChainLength,
    CK_BYTE_PTR * crls,
    CK_ULONG_PTR crlsLengths,
    CK_ULONG crlsLength
);
#endif

/**
* Сформировать запрос на сертификат.
* @param session PKCS#11 сессия.
* @param publicKey открытый ключ для создания сертификата.
* @param dn distinguished name. В параметр должен передаваться массив строк. В первой строке должен располагаться тип поля в текстовой форме, или
*                OID, например, "CN". Во второй строке должно располагаться значение поля в UTF8.
*                Последующие поля передаются в следующих строках. Количество строк должно быть четным.
* @param dnLength количество строк в dn.
* @param csr указатель на указатель на буфер в который будет записан запрос на сертификат.
*                Буфер создается внутри функции. После окончания работы с ним необходимо освободить его, вызвав функцию freeBuffer().
* @param csrLength длина буфера в который будет записан запрос на сертификат.
* @param privateKey закрытый ключ, парный publicKey. Если значение установленно в 0, то поиск закрытого ключа будет осуществляться
*                    по CKA_ID открытого ключа.
* @param attributes дополнительные атрибуты для включения в запрос. Формат аналогичен dn.
* @param attributesLength количество строк в attributes.
* @param extensions расширения для включения в запрос. Формат аналогичен dn.
* @param extensionsLength количество строк в extensions.
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(createCSR)
#ifdef CK_NEED_ARG_LIST
(
    CK_SESSION_HANDLE session,
    CK_OBJECT_HANDLE publicKey,
    CK_CHAR_PTR * dn,
    CK_ULONG dnLength,
    CK_BYTE_PTR * csr,
    CK_ULONG_PTR csrLength,
    CK_OBJECT_HANDLE privateKey,
    CK_CHAR_PTR * attributes,
    CK_ULONG attributesLength,
    CK_CHAR_PTR * extensions,
    CK_ULONG extensionsLength
);
#endif

/**
* Расширенное формирование запроса на сертификат.
* @param session PKCS#11 сессия.
* @param publicKey открытый ключ для создания сертификата.
* @param dn distinguished name. В параметр должен передаваться массив строк. В первой строке должен располагаться тип поля в текстовой форме, или
*               OID, например, "CN". Во второй строке должно располагаться значение поля в UTF8.
*               Последующие поля передаются в следующих строках. Количество строк должно быть четным.
* @param dnLength количество строк в dn.
* @param csr указатель на указатель на буфер в который будет записан запрос на сертификат.
                Буфер создается внутри функции. После окончания работы с ним необходимо освободить его, вызвав функцию freeBuffer().
* @param csrLength длина буфера в который будет записан запрос на сертификат.
* @param privateKey закрытый ключ, парный publicKey. Если значение установленно в 0, то поиск закрытого ключа будет осуществляться
                    по CKA_ID открытого ключа.
* @param attributes дополнительные атрибуты для включения в запрос. Формат аналогичен dn.
* @param attributesLength количество строк в attributes.
* @param extensions расширения для включения в запрос. Формат аналогичен dn.
* @param extensionsLength количество строк в extensions.
* @param signatureMech указатель на механизм подписи
* @return код ошибки
*/

CK_PKCS11_FUNCTION_INFO(createCSREx)
#ifdef CK_NEED_ARG_LIST
(
    CK_SESSION_HANDLE session,
    CK_OBJECT_HANDLE publicKey,
    CK_CHAR_PTR * dn,
    CK_ULONG dnLength,
    CK_BYTE_PTR * csr,
    CK_ULONG_PTR csrLength,
    CK_OBJECT_HANDLE privateKey,
    CK_CHAR_PTR * attributes,
    CK_ULONG attributesLength,
    CK_CHAR_PTR * extensions,
    CK_ULONG extensionsLength,
    CK_MECHANISM_PTR signatureMech
);
#endif

/**
* Расширенное формирование запроса на сертификат при помощи сторонних реализаций PKCS#11.
* @param pFunctionList функции PKCS#11
* @param session PKCS#11 сессия.
* @param publicKey открытый ключ для создания сертификата.
* @param dn distinguished name. В параметр должен передаваться массив строк. В первой строке должен располагаться тип поля в текстовой форме, или
*               OID, например, "CN". Во второй строке должно располагаться значение поля в UTF8.
*               Последующие поля передаются в следующих строках. Количество строк должно быть четным.
* @param dnLength количество строк в dn.
* @param csr указатель на указатель на буфер в который будет записан запрос на сертификат.
*               Буфер создается внутри функции. После окончания работы с ним необходимо освободить его, вызвав функцию freeBuffer().
* @param csrLength длина буфера в который будет записан запрос на сертификат.
* @param privateKey закрытый ключ, парный publicKey. Если значение установленно в 0, то поиск закрытого ключа будет осуществляться
*                   по CKA_ID открытого ключа.
* @param attributes дополнительные атрибуты для включения в запрос. Формат аналогичен dn.
* @param attributesLength количество строк в attributes.
* @param extensions расширения для включения в запрос. Формат аналогичен dn.
* @param extensionsLength количество строк в extensions.
* @param signatureMech указатель на механизм подписи
* @return код ошибки
*/

CK_PKCS11_FUNCTION_INFO(JC_CreateCertificateRequest)
#ifdef CK_NEED_ARG_LIST
(
    CK_FUNCTION_LIST_PTR pFunctionList,
    CK_SESSION_HANDLE session,
    CK_OBJECT_HANDLE publicKey,
    CK_CHAR_PTR * dn,
    CK_ULONG dnLength,
    CK_BYTE_PTR * csr,
    CK_ULONG_PTR csrLength,
    CK_OBJECT_HANDLE privateKey,
    CK_CHAR_PTR * attributes,
    CK_ULONG attributesLength,
    CK_CHAR_PTR * extensions,
    CK_ULONG extensionsLength,
    CK_MECHANISM_PTR signatureMech
);
#endif

/**
* Проверить подпись в запросе на сертификат.
* @param pReques запрос на сертификат.
* @param ulRequestSize длина запроса на сертификат
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(verifyReq)
#ifdef CK_NEED_ARG_LIST
(
    CK_BYTE_PTR pRequest,
    CK_ULONG ulRequestSize
);
#endif

/**
* Получить определенные данные их x509 сертификата
* @param pX509data сертификат в формате x509
* @param ulX509dataSize его длина
* @param dataType тип получаемых данных:
    X509_SUBJECT (0x01) получить владельца сертификата (нуль терминированная строка)
    X509_ISSUER (0x02)  получить издателя сертификата (нуль терминированная строка)
    X509_SERIAL (0x03)  получить серийный номер сертификата
* @param pOutputdata выходные данные
* @param pulOutputdataSize их длина
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(getCertificateAttribute)
#ifdef CK_NEED_ARG_LIST
(
    CK_BYTE_PTR pX509data,
    CK_ULONG ulX509dataSize,
    JC_EX_X509_DATA_TYPE dataType,
    CK_BYTE_PTR * pOutputdata,
    CK_ULONG_PTR pulOutputdataSize
);
#endif

/**
* Расширенная проверка подпись в запросе на сертификат. Подпись может проверятся на токене
* @param hSession PKCS#11 сессия.
* @param hPublicKey открытый ключ из ключевой пары, использованной при создании запроса на сертификат
* @param pCsr запрос на сертификат.
* @param ulCsrLength длина запроса на сертификат
* @param pMechanism механизм проверки подписи
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(verifyReqEx)
#ifdef CK_NEED_ARG_LIST
(
    CK_SESSION_HANDLE hSession,
    CK_OBJECT_HANDLE hPublicKey,
    CK_BYTE_PTR pCsr,
    CK_ULONG ulCsrLength,
    CK_MECHANISM_PTR pMechanism
);
#endif

/**
* Создать сертификат из запроса.
* @param session PKCS#11 сессия.
* @param csr запрос на сертификат.
* @param csrLength длина запроса на сертификат
* @param privateKey закрытый ключ издателя сертификата
* @param serial серийный номер сертификата в строковом представлении
* @param issuerDN distinguished name издателя сертификата. В параметр должен передаваться массив строк. В первой строке должен располагаться тип поля в текстовой форме,
                    или OID, например, "CN". Во второй строке должно располагаться значение поля в UTF8.
                    Если issuerDN равно нулю, distinguished name издателя устанавливается равным distinguished namе субъекта.
* @param issuerDNLength количество строк в issuerDN.
* @param days срок действия сертификата в днях.
* @param certificate указатель на указатель на буфер в который будет записан сертификат.
                        Буфер создается внутри функции. После окончания работы с ним необходимо освободить его, вызвав функцию freeBuffer().
* @param certificateLength длина буфера в который будет записан сертификат.
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(genCert)
#ifdef CK_NEED_ARG_LIST
(
    CK_SESSION_HANDLE session,
    CK_BYTE_PTR csr,
    CK_ULONG csrLength,
    CK_OBJECT_HANDLE privateKey,
    CK_CHAR_PTR serial,
    CK_CHAR_PTR * issuerDN,
    CK_ULONG issuerDNLength,
    CK_ULONG days,
    CK_BYTE_PTR * certificate,
    CK_ULONG_PTR certificateLength
);
#endif

/**
* Расширенное создание сертификата из запроса
* @param session PKCS#11 сессия.
* @param csr запрос на сертификат.
* @param csrLength длина запроса на сертификат
* @param privateKey закрытый ключ издателя сертификата
* @param publicKey открытый ключ издателя сертификата
* @param serial серийный номер сертификата в строковом представлении
* @param issuerDN distinguished name издателя сертификата. В параметр должен передаваться массив строк. В первой строке должен располагаться тип поля в текстовой форме,
                    или OID, например, "CN". Во второй строке должно располагаться значение поля в UTF8.
                    Если issuerDN равно нулю, distinguished name издателя устанавливается равным distinguished namе субъекта.
* @param issuerDNLength количество строк в issuerDN.
* @param days срок действия сертификата в днях.
* @param certificate указатель на указатель на буфер в который будет записан сертификат.
                        Буфер создается внутри функции. После окончания работы с ним необходимо освободить его, вызвав функцию freeBuffer().
* @param certificateLength длина буфера в который будет записан сертификат.
* @param mech механизм подписи
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(genCertEx)
#ifdef CK_NEED_ARG_LIST
(
    CK_SESSION_HANDLE session,
    CK_BYTE_PTR csr,
    CK_ULONG csrLength,
    CK_OBJECT_HANDLE privateKey,
    CK_OBJECT_HANDLE publicKey,
    CK_CHAR_PTR serial,
    CK_CHAR_PTR * issuerDN,
    CK_ULONG issuerDNLength,
    CK_ULONG days,
    CK_BYTE_PTR * certificate,
    CK_ULONG_PTR certificateLength,
    CK_MECHANISM_PTR mech
);
#endif

/**
* Получить информацию о сертификате в текстовом виде.
* @param session PKCS#11 сессия.
* @param certificate сертификат.
* @param certificateInfo указатель на указатель на буфер в который будет записана информация о сертификате.
                         Буфер создается внутри функции. После окончания работы с ним необходимо освободить его, вызвав функцию freeBuffer().
* @param certificateInfoLength длина буфера в который будет записана информация о сертификате.
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(getCertificateInfo)
#ifdef CK_NEED_ARG_LIST
(
    CK_SESSION_HANDLE session,
    CK_OBJECT_HANDLE certificate,
    CK_CHAR_PTR * certificateInfo,
    CK_ULONG * certificateInfoLength
);
#endif

/**
* Формирование PKCS#7 сообщения типа signed data.
* @param session PKCS#11 сессия.
* @param data данные для подписи.
* @param dataLength длина данных для подписи.
* @param signCertificate сертификат создателя сообщения в DER кодировке (массив байт).
* @param signCertificateLength длина signCertificate.
* @param envelope указатель на указатель на буфер в который будет записано сообщение.
                  Буфер создается внутри функции. После окончания работы с ним необходимо освободить его, вызвав функцию freeBuffer().
* @param envelopeLength указатель на длину созданного буфера с сообщением.
* @param privateKey закрытый ключ создателя сообщения. Может устанавливаться в 0, тогда поиск закрытого ключа будет осуществлятся по CKA_ID сертификата.
* @param certificates указатель на массив сертификатов, которые следует добавить в сообщение.
* @param certificatesLength количество сертификатов в параметре certificates.
* @param flags флаги. Может принимать значение 0 и PKCS7_DETACHED_SIGNATURE.
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(pkcs7SignEx)
#ifdef CK_NEED_ARG_LIST
(
    CK_SESSION_HANDLE session,
    CK_BYTE_PTR data,
    CK_ULONG dataLength,
    CK_BYTE_PTR signCertificate,
    CK_ULONG signCertificateLength,
    CK_BYTE_PTR * envelope,
    CK_ULONG_PTR envelopeLength,
    CK_OBJECT_HANDLE privateKey,
    CK_OBJECT_HANDLE_PTR certificates,
    CK_ULONG certificatesLength,
    CK_ULONG flags
);
#endif

/**
* Получить информацию о сертификате в текстовом виде.
* @param certificate сертификат в DER кодировке (массив байт).
* @param certificateLength длина certificate.
* @param certificateInfo указатель на указатель на буфер в который будет записана информация о сертификате.
                           Буфер создается внутри функции. После окончания работы с ним необходимо освободить его, вызвав функцию freeBuffer().
* @param certificateInfoLength длина буфера в который будет записана информация о сертификате.
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(getCertificateInfoEx)
#ifdef CK_NEED_ARG_LIST
(
    CK_BYTE_PTR certificate,
    CK_ULONG certificateLength,
    CK_CHAR_PTR * certificateInfo,
    CK_ULONG * certificateInfoLength
);
#endif

/**
* Освободить буфер, выделенный в одной из других функций.
* @param pBuffer буфер.
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(freeBuffer)
#ifdef CK_NEED_ARG_LIST
(
    CK_BYTE_PTR pBuffer
);
#endif

/**
* Установить использование аппаратного вычисления значения хеш-функции в функциях расширения.
* @param hardware - CK_TRUE - использовать аппаратное хеширование; CK_FALSE - использовать программное хеширование.
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(useHardwareHash)
#ifdef CK_NEED_ARG_LIST
(
    CK_BBOOL hardware
);
#endif

/**
* Начать установку TLS соединения. Функция может возвращать код CKR_NEED_MORE_DATA, если установка соединения со стороны сервера не завершена, и требуются дополнительные данные от сервера.
* @param pContext контекст соединения. После окончания работы с контекстом необходимо освободить его, вызвав функцию TLSCloseConnection().
* @param session PKCS#11 сессия.
* @param certificate сертификат.
* @param privateKey закрытый ключ.
* @param dataIn данные полученные от другой стороны (NULL_PTR в случае работы в режиме TLS клиента).
* @param dataInLength длина данных, полученных от другой стороны (0 в случае работы в режиме TLS клиента).
* @param dataOut данные для передачи другой стороне
* @param dataOutLength длина данных для передачи другой стороне (при вызове значение на которое указывает указатель должно содержать доступную длину буфера dataOut)
* @param serverMode устанавливать соединение в режиме TLS сервера (иначе в режиме TLS клиента)
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(TLSEstablishConnectionBegin)
#ifdef CK_NEED_ARG_LIST
(
    CK_VOID_PTR_PTR pContext,
    CK_SESSION_HANDLE session,
    CK_OBJECT_HANDLE certificate,
    CK_OBJECT_HANDLE privateKey,
    CK_BYTE_PTR data,
    CK_ULONG dataLength,
    CK_BYTE_PTR dataOut,
    CK_ULONG_PTR dataOutLength,
    CK_BBOOL serverMode
);
#endif

/**
* Продолжить установку TLS соединения. Функция может возвращать код CKR_NEED_MORE_DATA, если установка соединения со стороны сервера не завершена, и требуются дополнительные данные от сервера.
* @param pContext контекст соединения.
* @param dataIn данные полученные от другой стороны.
* @param dataInLength длина данных, полученных от другой стороны.
* @param dataOut данные для передачи другой стороне
* @param dataOutLength длина данных для передачи другой стороне (при вызове значение на которое указывает указатель должно содержать доступную длину буфера dataOut)
* @param serverMode устанавливать соединение в режиме TLS сервера (иначе в режиме TLS клиента)
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(TLSEstablishConnectionContinue)
#ifdef CK_NEED_ARG_LIST
(
    CK_VOID_PTR pContext,
    CK_BYTE_PTR dataIn,
    CK_ULONG dataInLength,
    CK_BYTE_PTR dataOut,
    CK_ULONG_PTR dataOutLength,
    CK_BBOOL serverMode
);
#endif

/**
* Получить сертификат сервера (peer-а). Можно вызывать только после успешного установления соединения.
* @param pContext контекст соединения.
* @param certificate сертификат сервера.
* @param certificateLength длина сертификата сервера (при вызове значение на которое указывает указатель должно содержать доступную длину буфера dataOut).
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(TLSGetPeerCertificate)
#ifdef CK_NEED_ARG_LIST
(
    CK_VOID_PTR pContext,
    CK_BYTE_PTR certificate,
    CK_ULONG_PTR certificateLength
);
#endif

/**
* Получить значение открытого ключа сервера (peer-а). Можно вызывать только после успешного установления соединения.
* @param pContext контекст соединения.
* @param publicKeyValue открытый ключа.
* @param publicKeyValueLength длина открытого ключа сервера (при вызове значение на которое указывает указатель должно содержать доступную длину буфера dataOut).
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(TLSGetPeerPublicKeyValue)
#ifdef CK_NEED_ARG_LIST
(
    CK_VOID_PTR pContext,
    CK_BYTE_PTR publicKeyValue,
    CK_ULONG_PTR publicKeyValueLenght
);
#endif

/*
* Закодировать данные для передачи на сервер.
* @param pContext контекст соединения.
* @param dataIn данные для передачи.
* @param dataInLength длина данных для передачи.
* @param dataOut данные для передачи другой стороне. Не меньше 1 и не больше 0x4000 (16Kb).
* @param dataOutLength длина данных для передачи другой стороне (при вызове значение на которое указывает указатель должно содержать доступную длину буфера dataOut)
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(TLSEncodeData)
#ifdef CK_NEED_ARG_LIST
(
    CK_VOID_PTR pContext,
    CK_BYTE_PTR dataIn,
    CK_ULONG dataInLength,
    CK_BYTE_PTR dataOut,
    CK_ULONG_PTR dataOutLength
);
#endif

/*
* Раскодировать данные, пришедшие от сервера.
* @param pContext контекст соединения.
* @param dataIn данные от сервера.
* @param dataInLength длина данных от сервера.
* @param dataOut данные для передачи другой стороне. Не меньше 1 и не больше 0x4000 (16Kb).
* @param dataOutLength длина данных для передачи другой стороне (при вызове значение на которое указывает указатель должно содержать доступную длину буфера dataOut)
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(TLSDecodeData)
#ifdef CK_NEED_ARG_LIST
(
    CK_VOID_PTR pContext,
    CK_BYTE_PTR dataIn,
    CK_ULONG dataInLength,
    CK_BYTE_PTR dataOut,
    CK_ULONG_PTR dataOutLength
);
#endif

/*
* Закрыть TLS соединение
* @param pContext контекст соединения.
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(TLSCloseConnection)
#ifdef CK_NEED_ARG_LIST
(
    CK_VOID_PTR pContext
);
#endif

/**
* Получить счетчики ПИН-кодов для ГОСТ
* @param slotID идентификатор слота
* @param pPinCounters информация о счетчиках ПИН-кодов
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_CT1_ReadPinCounters)
#ifdef CK_NEED_ARG_LIST
(
    CK_SLOT_ID slotID,
    JC_CT1_PIN_COUNTERS_PTR pPinCounters
);
#endif

/**
* Получить счетчики ПИН-кодов для Datastore
* @param slotID идентификатор слота
* @param pPinCounters информация о счетчиках ПИН-кодов
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_DS_ReadPinCounters)
#ifdef CK_NEED_ARG_LIST
(
    CK_SLOT_ID slotID,
    JC_DS_PIN_COUNTERS_PTR pPinCounters
);
#endif

/**
* Получить расширенную информацию для WebPass
* @param slotID идентификатор слота
* @param pPinCounters расширенная информация о WebPass
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_WP_ReadExtInfo)
#ifdef CK_NEED_ARG_LIST
(
    CK_SLOT_ID slotID,
    JC_WP_INFO_PTR pInfo
);
#endif

/**
* @fn CK_RV JC_WP_ReadValue(CK_SESSION_HANDLE hSession, CK_OBJECT_HANDLE hObject, CK_BYTE_PTR pOutput, CK_ULONG_PTR pulOutputLength)
* Считать значение слота WebPass. Результирующее значение зависит от типа слота
* Тип слота       | Возвращаемое значение
* ----------------|-----------------------
* JC_WP_TYPE_OTP  | Случайный одноразовый пароль с префиксом (если префикс был указан при создании)
* JC_WP_TYPE_PASS | Многоразовый пароль
* JC_WP_TYPE_URL  | Адрес с префиксом в виде кода платформы и суффиксом в виде символа с кодом 0x0D
*
* @param hSession дескриптор сессии
* @param hObject дескриптор объекта
* @param pOutput буфер для считываемого значения
* @param pOutputLength размер буфера для считываемого значения
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_WP_ReadValue)
#ifdef CK_NEED_ARG_LIST
(
    CK_SESSION_HANDLE hSession,
    CK_OBJECT_HANDLE hObject,
    CK_BYTE_PTR pOutput,
    CK_ULONG_PTR pulOutputLength
);
#endif

/**
* Загрузить контейнер для виртуального токена
* @param type тип токена
* @param pFileName абсолютный путь до файла, содержащего контейнер
* @param ulFileNameSize длина пути в байтах. М.б. CK_UNAVAILABLE_INFORMATION, если путь заканчивается 0
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_VT_LoadContainer)
#ifdef CK_NEED_ARG_LIST
(
    JC_APPLET_TYPE type,
    CK_UTF8CHAR_PTR pFileName,
    CK_ULONG ulFileNameSize
);
#endif

/**
* Выгрузить контейнер для виртуального токена
* @param pFileName абсолютный путь до файла, содержащего контейнер
* @param ulFileNameSize длина пути в байтах. М.б. CK_UNAVAILABLE_INFORMATION, если путь заканчивается 0
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_VT_UnloadContainer)
#ifdef CK_NEED_ARG_LIST
(
    CK_UTF8CHAR_PTR pFileName,
    CK_ULONG ulFileNameSize
);
#endif

/**
* Проверить принадлежит ли слот виртуальному токену
* @param slotID идентификатор слота
* @param pVirtual CK_TRUE, если слот принадлежит виртуальному токену
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_VT_IsVirtual)
#ifdef CK_NEED_ARG_LIST
(
    CK_SLOT_ID slotID,
    CK_BBOOL_PTR pVirtual
);
#endif

/**
* Управление логированием библиотеки. М.б. вызвана до вызова C_Initialize
* @param mode режим логирования
* @param pFileName имя файла для лога. Каталоги должны существовать. М.б. указано stdout для вывода на консоль
* @param ulFileNameLength размер имени файла в байтах. М.б. равен CK_UNAVAILABLE_INFORMATION если имя файла заканчивается 0
*/
CK_PKCS11_FUNCTION_INFO(JC_SetLog)
#ifdef CK_NEED_ARG_LIST
(
    JC_LOG_MODE mode,
    CK_UTF8CHAR_PTR pFileName,
    CK_ULONG ulFileNameLength
);
#endif

/**
* Получить информацию о WebPass, заданную на этапе производства
* @param slotID идентификатор слота
* @param pPinCounters информация о WebPass, заданную на этапе производства
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_WP_ReadProductionInfo)
#ifdef CK_NEED_ARG_LIST
(
    CK_SLOT_ID slotID,
    JC_WP_PRODUCTION_INFO_PTR pInfo
);
#endif

/**
* Инициализировать установить значение атрибута защищенного объекта. Требует аутентификации пользователем.
* @param slotID идентификатор слота
* @param hSession дескриптор сессии
* @param hObject дескриптор объекта
* @param pAttribute адрес массива описателей устанавливаемых атрибутов объекта
* @param ulCount число описателей устанавливаемых атрибутов
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_CT1_SetAttributeValue)
#ifdef CK_NEED_ARG_LIST
(
    CK_SESSION_HANDLE hSession,
    CK_OBJECT_HANDLE hObject,
    CK_ATTRIBUTE_PTR pAttribute,
    CK_ULONG ulCount
);
#endif

/**
* Считать пароль профиля SecurLogon. Требует аутентификации пользователем
* @param hSession дескриптор сессии
* @param hProfile дескриптор о профиля SecurLogon
* @param pPassword буфер для пароля
* @param pulPasswordLength длина буфера пароля в байтах
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_SL_ReadPassword)
#ifdef CK_NEED_ARG_LIST
(
    CK_SESSION_HANDLE hSession,
    CK_OBJECT_HANDLE hProfile,
    CK_BYTE_PTR pPassword,
    CK_ULONG_PTR pulPasswordLength
);
#endif

/**
* Сохранить пароль профиля SecurLogon. Требует аутентификации пользователем
* @param hSession дескриптор сессии
* @param hProfile дескриптор о профиля SecurLogon
* @param pPassword пароль
* @param ulPasswordLength длина пароля в байтах
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_SL_WritePassword)
#ifdef CK_NEED_ARG_LIST
(
    CK_SESSION_HANDLE hSession,
    CK_OBJECT_HANDLE hProfile,
    CK_BYTE_PTR pPassword,
    CK_ULONG ulPasswordLength
);
#endif

/**
* Получить расширенную информацию об устройстве
* @param slotID идентификатор слота
* @param pExtendedInfo расширенная информация об устройстве
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_F2_GetExtendedInfo)
#ifdef CK_NEED_ARG_LIST
(
    CK_SLOT_ID slotID,
    JC_F2_EXTENDED_INFO_PTR pExtendedInfo
);
#endif

/**
* Установить жизненный цикл Flash2. Функция прототип, возможно ее изменение. Требуется аутентификация администратором.
* После успешного выполнения необходимо повтор найти токен, т.к. в процессе выполнения команды он отключается и подключается заново.
* Схема допустимых переключений жизненных циклов
*
* Текущий жизненный цикл         | Жизненный цикл, который можно установить
* -------------------------------|-------------------------------------------------------------------------------
* JC_F2_LIFE_CYCLE_EMPTY         | отсутствует, доступен только вызов JC_F2_Format
* JC_F2_LIFE_CYCLE_INITIALIZED   | JC_F2_LIFE_CYCLE_EMPTY, JC_F2_LIFE_CYCLE_READY
* JC_F2_LIFE_CYCLE_READY         | JC_F2_LIFE_CYCLE_EMPTY, JC_F2_LIFE_CYCLE_INITIALIZED (только до инициализации ключа шифрования скрытых разделов, см. JC_F2_EXTENDED_INFO.HasPartitionKey)
* JC_F2_LIFE_CYCLE_READY_MOUNTED | JC_F2_LIFE_CYCLE_READY - аналогично вызову JC_F2_UmountPrivateDisks(CK_FALSE)
*
* @param slotID идентификатор слота
* @param lifeCycle устанавливаемый жизненный цикл за исключением JC_F2_LIFE_CYCLE_READY_MOUNTED
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_F2_SetLifeCycle)
#ifdef CK_NEED_ARG_LIST
(
    CK_SLOT_ID slotID,
    JC_F2_LIFE_CYCLE lifeCycle
);
#endif

/**
* Форматировать устройство Flash2. Все размеры задаются в байтах, и д.б. округлены до 512 байт. Минимальный размер раздела JC_F2_MIN_PARTITION_SIZE байт
* М.б. выполнено только в жизненном цикле JC_F2_LIFE_CYCLE_EMPTY. Переводит устройство в жизненный цикл JC_F2_LIFE_CYCLE_INITIALIZED.
* После успешного выполнения необходимо повторно найти токен, т.к. в процессе выполнения команды он отключается и подключается заново.
* Требуется аутентификация администратором.
* @param slotID идентификатор слота
* @param ulPublicRWSize размер открытого RW раздела в байтах, д.б. округлен до 512 байт
* @param ulPublicCDSize размер открытого CDROM раздела в байтах, д.б. округлен до 512 байт
* @param ulPrivateCDSize размер скрытого CDROM раздела в байтах, д.б. округлен до 512 байт
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_F2_Format)
#ifdef CK_NEED_ARG_LIST
(
    CK_SLOT_ID slotID,
    CK_ULONG ulPublicRWSize,
    CK_ULONG ulPublicCDSize,
    CK_ULONG ulPrivateCDSize
);
#endif

/**
* Установить размеры образов, записываемых на раздел CDROM. Все размеры задаются в байтах, и д.б. округлены до 512 байт.
* М.б. выполнена только в жизненном цикле JC_F2_LIFE_CYCLE_INITIALIZED.
* Требуется аутентификация администратором.
* @param slotID идентификатор слота
* @param ulPublicISOSize размер образа, записываемый на открытый CDROM раздел
* @param ulPrivateISOSize размер образа, записываемый на скрытый CDROM раздел
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_F2_SetISOSizes)
#ifdef CK_NEED_ARG_LIST
(
    CK_SLOT_ID slotID,
    CK_ULONG ulPublicISOSize,
    CK_ULONG ulPrivateISOSize
);
#endif

/**
* Обновить прошивку антифрод терминала.
* @param slotID идентификатор слота
* @param pFirmWare адрес образа прошивки
* @param ulFirmWareSize размер образа прошивки
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_CT1_UpgradeVascoFW)
#ifdef CK_NEED_ARG_LIST
(
    CK_SLOT_ID slotID,
    CK_VOID_PTR pOS,
    CK_ULONG ulOSSize,
    CK_VOID_PTR pApplication,
    CK_ULONG ulApplicationSize
    );
#endif

/**
* Получить запрос, необходимый для подключения скрытого раздела. М.б. выполнена только в жизненном цикле JC_F2_LIFE_CYCLE_READY.
* Требуется аутентификация пользователем. М.б. выполнена только в жизненном цикле JC_F2_LIFE_CYCLE_READY.
* @param hSession дескриптор сессии
* @param hTokenKey дескриптор ключа токена
* @param pChallenge буфер для запроса не подключение скрытого раздела
* @param pulChallengeSize размер буфера запроса на подключение скрытого раздела в байтах
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_F2_GetMountChallenge)
#ifdef CK_NEED_ARG_LIST
(
    CK_SESSION_HANDLE hSession,
    CK_OBJECT_HANDLE hTokenKey,
    CK_BYTE_PTR pChallenge,
    CK_ULONG_PTR pulChallengeSize
);
#endif

/**
* Подключить скрытый раздел с использованием ответа на запрос, полученный при помощи JC_F2_CreateMountResponse.
* Переводит устройство в жизненный цикл JC_F2_LIFE_CYCLE_READY_MOUNTED.
* После успешного выполнения необходимо повторно найти токен, т.к. в процессе выполнения команды он отключается и подключается заново.
* Требуется аутентификация пользователем или администратором. М.б. выполнена только в жизненном цикле JC_F2_LIFE_CYCLE_READY.
* @param slotID идентификатор слота
* @param bForceMount CK_TRUE - подключить скрытый раздел без ожидания завершения операция чтения/записи. CK_FALSE - если активны операции чтения/записи данных, то будет возвращен код ошибки CKR_DEVICE_BUSY
* @param pResponse буфер ответа на запрос
* @param ulResponseSize длина ответа на запрос в байтах
* @param offlineMode CK_TRUE, если монтирование происходит в автономном режиме
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_F2_MountPrivateDisks)
#ifdef CK_NEED_ARG_LIST
(
    CK_SLOT_ID slotID,
    CK_BBOOL bForceMount,
    CK_BYTE_PTR pResponse,
    CK_ULONG ulResponseSize,
    CK_BBOOL offlineMode
);
#endif

/**
* Отключить скрытый раздел.
* Переводит устройство в жизненный цикл JC_F2_LIFE_CYCLE_READY.
* После успешного выполнения необходимо повторно найти токен, т.к. в процессе выполнения команды он отключается и подключается заново.
* Требуется аутентификация пользователем или администратором при отключении без ожидания. М.б. выполнена только на жизненном цикле JC_F2_LIFE_CYCLE_READY_MOUNTED
* @param slotID идентификатор слота
* @param bForceMount CK_TRUE - отключить скрытый раздел без ожидания завершения операция чтения/записи. CK_FALSE - если активны операции чтения/записи данных, то будет возвращен код ошибки CKR_DEVICE_BUSY
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_F2_UmountPrivateDisks)
#ifdef CK_NEED_ARG_LIST
(
    CK_SLOT_ID slotID,
    CK_BBOOL bForceUmount
);
#endif

/**
* Создать ответ на запрос подключения скрытого раздела, полученного при помощи функции JC_F2_GetMountChallenge
* Требуется аутентификация пользователем. М.б. выполнена только на жизненном цикле JC_F2_LIFE_CYCLE_READY
* @param hSession дескриптор сессии
* @param hMasterKey дескриптор мастер-ключа
* @param pChallange запроса на подключение скрытого раздела
* @param ulChallengeSize размер запроса на подключение скрытого раздела в байтах
* @param pResponse буфер для ответа на запрос
* @param pulResponseSize размер буфера для ответа на запрос в байтах
*/
CK_PKCS11_FUNCTION_INFO(JC_F2_CreateMountResponse)
#ifdef CK_NEED_ARG_LIST
(
    CK_SESSION_HANDLE hSession,
    CK_OBJECT_HANDLE hMasterKey,
    CK_BYTE_PTR pChallange,
    CK_ULONG ulChallengeSize,
    CK_BYTE_PTR pResponse,
    CK_ULONG_PTR pulResponseSize
);
#endif

/**
* Инициализировать шифрование разделов токена.
* Требуется аутентификация администратором. М.б. выполнена только на жизненном цикле JC_F2_LIFE_CYCLE_INITIALIZED. Данные для инициализации
* для токена администратора генерируются случайно с размером 32 байта, для токена пользователя получаются как результат выполнения функции JC_F2_CreateInitResponse
* @param slotID идентификатор слота
* @param tokenType тип инициализации токена
* @param algorithm алгоритм шифрования скрытых разделов
* @param pInitData данные инициализации
* @param ulInitDataSize размер данных инициализации в байтах
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_F2_InitPartitionKey)
#ifdef CK_NEED_ARG_LIST
(
    CK_SLOT_ID slotID,
    JC_F2_TOKEN_TYPE tokenType,
    JC_F2_ALGORITHM_TYPE algorithm,
    CK_VOID_PTR pInitData,
    CK_ULONG ulInitDataSize
);
#endif

/**
* Установить ПУК-код для Flash2. Требуется аутентификация администратором.
* Требуется аутентификация администратором.
* @param slotID идентификатор слота
* @param pPin ПУК-код
* @param ulPinSize размер ПУК-кода в байтах. М.б. CK_UNAVAILABLE_INFORMATION если ПУК-код заканчивается 0
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_F2_SetPUK)
#ifdef CK_NEED_ARG_LIST
(
    CK_SLOT_ID slotID,
    CK_UTF8CHAR_PTR pPuk,
    CK_ULONG ulPukSize
);
#endif

/**
* Установить политику ПИН-кода. Требуется аутентификация администратором.
* Требуется аутентификация администратором.
* @param slotID идентификатор слота
* @param pPinPolicy политика ПИН-кода
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_F2_SetPINPolicy)
#ifdef CK_NEED_ARG_LIST
(
    CK_SLOT_ID slotID,
    JC_F2_PIN_TYPE pinType,
    JC_F2_PIN_POLICY_PTR pPinPolicy
);
#endif

/**
* Сохранить описание токена. При задании tokenType == CK_UNAVAILABLE_INFORMATION и pDescription == NULL и ulDescriptionSize == 0 информация об описании токена удаляется
* Требуется аутентификация администратором.
* @param slotID идентификатор слота
* @param tokenType тип токена
* @param pDescription описание токена
* @param ulDescriptionSize размер описания токена в байтах. М.б. равен CK_UNAVAILABLE_INFORMATION если описание заканчивается 0
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_F2_SetDescription)
#ifdef CK_NEED_ARG_LIST
(
    CK_SLOT_ID slotID,
    JC_F2_TOKEN_TYPE tokenType,
    CK_UTF8CHAR_PTR pDescription,
    CK_ULONG ulDescriptionSize
);
#endif

/**
* Прочитать описание токена
* @param slotID идентификатор слота
* @param pTokenType тип токена
* @param pDescription буфер для описания токена
* @param pulDescriptionSize размер буфера для описания токена в байтах.
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_F2_GetDescription)
#ifdef CK_NEED_ARG_LIST
(
    CK_SLOT_ID slotID,
    JC_F2_TOKEN_TYPE_PTR pTokenType,
    CK_UTF8CHAR_PTR pDescription,
    CK_ULONG_PTR pulDescriptionSize
);
#endif

/**
* Попытаться восстановить состояние токена после ошибки CKR_BROKEN_STATE
* Требуется аутентификация администратором.
* @param slotID идентификатор слота
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_F2_RestoreState)
#ifdef CK_NEED_ARG_LIST
(
    CK_SLOT_ID slotID
);
#endif

/**
* Создать запрос для инициализации токена
* @param slotID идентификатор слота
* @param pChallenge буфер для запроса на инициализацию токена
* @param pulChallengeSize размер буфера для запроса на инициализацию токена в байтах
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_F2_GetInitChallenge)
#ifdef CK_NEED_ARG_LIST
(
    CK_SLOT_ID slotID,
    CK_BYTE_PTR pChallenge,
    CK_ULONG_PTR pulChallengeSize
);
#endif

/**
* Создать ответ на запрос инициализации токена, полученный при помощи функции JC_F2_GetInitChallenge
* Требуется аутентификация администратором. М.б. выполнена только на жизненном цикле JC_F2_LIFE_CYCLE_READY
* @param slotID идентификатор слота
* @param pChallenge запрос на инициализацию токена
* @param ulChallengeSize размер запроса на инициализацию токена в байтах
* @param pResponse данные для инициализации токена пользователя
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_F2_CreateInitResponse)
#ifdef CK_NEED_ARG_LIST
(
    CK_SLOT_ID slotID,
    CK_BYTE_PTR pChallenge,
    CK_ULONG ulChallengeSize,
    JC_F2_INIT_RESPONSE_PTR pResponse
);
#endif

/**
* Вычислить ответ на запрос подключения скрытых разделов без использования токена
* @param pChallenge информация о запросе на подключение скрытых разделов
* @param pResponse буфер для ответа на запрос
* @param pulResponseSize размер буфера для ответа на запрос в байтах
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_F2_CreateMountResponseSW)
#ifdef CK_NEED_ARG_LIST
(
    JC_F2_MOUNT_CHALLENGE_INFO_PTR pChallenge,
    CK_BYTE_PTR pResponse,
    CK_ULONG_PTR pulResponseSize
);
#endif

/**
* Установить режим перезаписи скрытых разделов. Ключ шифрования раздела - это результат выполнения функции JC_F2_CreateInitResponse: JC_F2_INIT_RESPONSE.PartitionKey
* @param slotID идентификатор слота
* @param pPartitionKey значение ключа шифрования скрытых разделов
* @param ulPartitionKeySize размер ключа шифрования скрытых разделов в байтах
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_F2_EnableISORewrite)
#ifdef CK_NEED_ARG_LIST
(
    CK_SLOT_ID slotID,
    CK_BYTE_PTR pPartitionKey,
    CK_ULONG ulPartitionKeySize
);
#endif

/**
* Подготовить токен к автономному монтированию
* Требуется аутентификация администратором.
* @param slotID идентификатор слота
* @param pChallenge буфер для запроса на автономное монтирование токена
* @param pulChallengeSize размер буфера для запроса на автономное монтирование токена в байтах
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_F2_GetOfflineMountChallenge)
#ifdef CK_NEED_ARG_LIST
(
    CK_SLOT_ID slotID,
    CK_BYTE_PTR pChallenge,
    CK_ULONG_PTR pulChallengeSize
);
#endif

/**
* Создать ответ на запрос автономного монтирования токена, полученный при помощи функции JC_F2_GetOfflineMountChallenge
* @param pAuthorizationKey мастер ключ авторизации
* @param ulAurthorizationKeySize размер мастер ключа авторизации в байтах
* @param pChallenge запрос на автономное монтирование токена
* @param ulChallengeSize размер запроса на автономное монтирование токена в байтах
* @param pResponse буфер для ответа на запрос автономного монтирования
* @param pulResponseSize размер буфера для ответа на запрос автономного монтирования в байтах
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_F2_CreateOfflineMountResponse)
#ifdef CK_NEED_ARG_LIST
(
    CK_BYTE_PTR pAuthorizationKey,
    CK_ULONG ulAurthorizationKeySize,
    CK_BYTE_PTR pChallenge,
    CK_ULONG ulChallengeSize,
    CK_BYTE_PTR pResponse,
    CK_ULONG_PTR pulResponseSize
);
#endif

/**
* Записать и выбрать ключ авторизации. При пере подключении или отключении токена активным становится ключ авторизации со значением JC_MIN_AUTHORIZATION_KEY_INDEX. Ключ с таким индексом
* записывается на токен при вызове функции JC_F2_InitPartitionKey.
* Требуется аутентификация администратором или пользователем.
* @param slotID идентификатор слота
* @param ulKeyIndex индекс ключа в пределах [JC_MIN_AUTHORIZATION_KEY_INDEX, JC_MAX_AUTHORIZATION_KEY_INDEX]. При значении индекса JC_MIN_AUTHORIZATION_KEY_INDEX задавать значение ключа запрещено
* @param pAuthorizationKey ключ авторизации. М.б. равен NULL, если необходимо выбрать уже существующий ключ
* @param ulAurthorizationKeySize размер ключа авторизации в байтах. М.б. равен 0, если необходимо выбрать уже существующий ключ
* @return код ошибки
*/
CK_PKCS11_FUNCTION_INFO(JC_F2_SetAuthorizationKey)
#ifdef CK_NEED_ARG_LIST
(
    CK_SLOT_ID slotID,
    CK_ULONG ulKeyIndex,
    CK_BYTE_PTR pAuthorizationKey,
    CK_ULONG ulAurthorizationKeySize
);
#endif
