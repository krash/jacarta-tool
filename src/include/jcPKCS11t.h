#ifndef JC_PKCS11_TYPES_H
#define JC_PKCS11_TYPES_H 1

/* константы TK26 */
#define NSSCK_VENDOR_PKCS11_RU_TEAM             0xD4321000 /* 0x80000000 | 0x54321000 */
#define CK_VENDOR_PKCS11_RU_TEAM_TK26           NSSCK_VENDOR_PKCS11_RU_TEAM

#define CKK_GOSTR3410_512                       (CK_VENDOR_PKCS11_RU_TEAM_TK26 | 0x003)
#define CKM_GOSTR3410_512_KEY_PAIR_GEN          (CK_VENDOR_PKCS11_RU_TEAM_TK26 | 0x005)
#define CKM_GOSTR3410_512                       (CK_VENDOR_PKCS11_RU_TEAM_TK26 | 0x006)
#define CKM_GOSTR3410_12_DERIVE                 (CK_VENDOR_PKCS11_RU_TEAM_TK26 | 0x007)
#define CKM_GOSTR3410_WITH_GOSTR3411_12_256     (CK_VENDOR_PKCS11_RU_TEAM_TK26 | 0x008)
#define CKM_GOSTR3410_WITH_GOSTR3411_12_512     (CK_VENDOR_PKCS11_RU_TEAM_TK26 | 0x009)
#define CKM_GOSTR3410_PUBLIC_KEY_DERIVE         (CK_VENDOR_PKCS11_RU_TEAM_TK26 | 0x00A)
#define CKM_GOSTR3411_12_256                    (CK_VENDOR_PKCS11_RU_TEAM_TK26 | 0x012)
#define CKM_GOSTR3411_12_512                    (CK_VENDOR_PKCS11_RU_TEAM_TK26 | 0x013)
#define CKM_GOSTR3411_12_256_HMAC               (CK_VENDOR_PKCS11_RU_TEAM_TK26 | 0x014)
#define CKM_GOSTR3411_12_512_HMAC               (CK_VENDOR_PKCS11_RU_TEAM_TK26 | 0x015)

/* структуры и типы ГОСТ не определенные в файлах спецификации 2.3 */
#define CKM_TLS_GOST_PRF                        (NSSCK_VENDOR_PKCS11_RU_TEAM |0x030)
#define CKM_TLS_GOST_PRE_MASTER_KEY_GEN         (NSSCK_VENDOR_PKCS11_RU_TEAM |0x031)
#define CKM_TLS_GOST_MASTER_KEY_DERIVE          (NSSCK_VENDOR_PKCS11_RU_TEAM |0x032)
#define CKM_TLS_GOST_KEY_AND_MAC_DERIVE         (NSSCK_VENDOR_PKCS11_RU_TEAM |0x033)

#define CKD_CPDIVERSIFY_KDF     0x00000009

typedef struct CK_GOSTR3410_KEY_WRAP_PARAMS
{
    CK_BYTE_PTR      pWrapOID;
    CK_ULONG         ulWrapOIDLen;
    CK_BYTE_PTR      pUKM;
    CK_ULONG         ulUKMLen;
    CK_OBJECT_HANDLE hKey;
} CK_GOSTR3410_KEY_WRAP_PARAMS;

typedef CK_GOSTR3410_KEY_WRAP_PARAMS CK_PTR CK_GOSTR3410_KEY_WRAP_PARAMS_PTR;

typedef struct CK_GOSTR3410_DERIVE_PARAMS
{
    CK_EC_KDF_TYPE  kdf;
    CK_BYTE_PTR     pPublicData;
    CK_ULONG        ulPublicDataLen;
    CK_BYTE_PTR     pUKM;
    CK_ULONG        ulUKMLen;
} CK_GOSTR3410_DERIVE_PARAMS;

typedef CK_GOSTR3410_DERIVE_PARAMS CK_PTR CK_GOSTR3410_DERIVE_PARAMS_PTR;

typedef struct CK_TLS_GOST_PRF_PARAMS
{
    CK_TLS_PRF_PARAMS TlsPrfParams;
    CK_BYTE_PTR pHashParamsOID;
    CK_ULONG ulHashParamsOIDLen;
} CK_TLS_GOST_PRF_PARAMS;
typedef CK_TLS_GOST_PRF_PARAMS CK_PTR CK_TLS_GOST_PRF_PARAMS_PTR;

typedef struct CK_TLS_GOST_MASTER_KEY_DERIVE_PARAMS
{
    CK_SSL3_RANDOM_DATA RandomInfo;
    CK_BYTE_PTR pHashParamsOID;
    CK_ULONG ulHashParamsOIDLen;
} CK_TLS_GOST_MASTER_KEY_DERIVE_PARAMS;
typedef CK_TLS_GOST_MASTER_KEY_DERIVE_PARAMS CK_PTR CK_TLS_GOST_MASTER_KEY_DERIVE_PARAMS_PTR;

typedef struct CK_TLS_GOST_KEY_MAT_PARAMS
{
    CK_SSL3_KEY_MAT_PARAMS KeyMatParams;
    CK_BYTE_PTR pHashParamsOID;
    CK_ULONG ulHashParamsOIDLen;
} CK_TLS_GOST_KEY_MAT_PARAMS;
typedef CK_TLS_GOST_KEY_MAT_PARAMS CK_PTR CK_TLS_GOST_KEY_MAT_PARAMS_PTR;

/* потерянные типы - есть в документации, но нет в файле */
#ifndef CKA_NAME_HASH_ALGORITHM
#define CKA_NAME_HASH_ALGORITHM     0x0000008C
#endif
#ifndef CKA_COPYABLE
#define CKA_COPYABLE                0x00000171
#endif

/* специальные константы для Laser */
#define CKA_COMPRESSED      (0xC000)

/* дополнительные типы для наших нужд */
typedef struct JC_FUNCTION_LIST JC_FUNCTION_LIST;

typedef JC_FUNCTION_LIST CK_PTR JC_FUNCTION_LIST_PTR;

typedef JC_FUNCTION_LIST_PTR CK_PTR JC_FUNCTION_LIST_PTR_PTR;

typedef CK_BBOOL CK_PTR CK_BBOOL_PTR;

typedef CK_BYTE_PTR CK_PTR CK_BYTE_PTR_PTR;

/** Старшая версия библиотеки. */
#define JC_VERSION_MAJOR 0x02
/** Младшая версия библиотеки. */
#define JC_VERSION_MINOR 0x01

/** Старшая версия расширений библиотеки. */
#define JC_EXTENSION_VERSION_MAJOR 0x02
/** Младшая версия расширений библиотеки. */
#define JC_EXTENSION_VERSION_MINOR 0x04

/** Идентификатор производителя */
#define JC_MANUFACTURER_ID "Aladdin R.D."

/**
* апплет DataStore (STORAGE)
*/
#define JC_MODEL_DATASTORE "JaCarta DS"

/**
* апплет Криптотокен1 (ГОСТ)
*/
#define JC_MODEL_CRYPTOTOKEN_1 "eToken GOST"

/**
* апплет Криптотокен2 (ГОСТ)
*/
#define JC_MODEL_CRYPTOTOKEN_2 "JaCarta GOST 2.0"

/**
* апплет Криптотокен1 (ГОСТ)
*/
#define JC_MODEL_LASER "JaCarta Laser"

/**
* Антифрод-терминал без карты
*/
#define JC_MODEL_ANTIFRAUD "Antifraud"

/**
* WebPass
*/
#define JC_MODEL_WEBPASS "JaCarta WebPass"

/**
* Flash 2
*/
#define JC_MODEL_FLASH2 "JaCarta Flash2"

/**
* EToken PRO for secur logon
*/
#define JC_MODEL_ETOKEN_SL_PRO "ETokenSLPro"

/**
* EToken PRO Java for secur logon
*/
#define JC_MODEL_ETOKEN_SL_PRO_JAVA "ETokenSLProJava"

/**
* атрибут виртуальных ключей типа CK_BBOOL - признак активности ключа
*/
#define CKA_CT2_KEY_ACTIVE          (CKA_VENDOR_DEFINED | 0x0001)
/**
* атрибут объекта WebPass типа CK_ULONG - жизненный цикл слота - одно из значений JC_WP_LIFECYCLE
*/
#define CKA_WP_LIFECYCLE            (CKA_VENDOR_DEFINED | 0x0002)
/**
* атрибут объекта WebPass типа CK_ULONG - тип слота - одно из значений JC_WP_TYPE
*/
#define CKA_WP_TYPE                 (CKA_VENDOR_DEFINED | 0x0003)
/**
* атрибут объекта WebPass типа CK_ULONG - алгоритм одноразового пароля - одно из значений JC_WP_OTP_ALGORITHM
*/
#define CKA_WP_OTP_ALGORITHM        (CKA_VENDOR_DEFINED | 0x0004)
/**
* атрибут объекта WebPass типа CK_ULONG - текущее значение счетчика генераций
*/
#define CKA_WP_OTP_COUNTER          (CKA_VENDOR_DEFINED | 0x0005)
/**
* атрибут объекта WebPass - название слота (в кодировке UTF-8) длиной не более 32 байт
*/
#define CKA_WP_NAME                 (CKA_VENDOR_DEFINED | 0x0006)
/**
* атрибут объекта WebPass  - префикс одноразового пароля длиной не более JC_WP_MAX_PREFIX_LENGTH байт.Допустимы английские буквы, цифры и следующие символы: !$%&'()*+,-./:;=?@_~
*/
#define CKA_WP_OTP_PREFIX           (CKA_VENDOR_DEFINED | 0x0007)
/**
* атрибут объекта WebPass типа CK_ULONG - качество многоразового пароля - маска значений JC_WP_PASS_QUALITY
*/
#define CKA_WP_PASS_QUALITY         (CKA_VENDOR_DEFINED | 0x0008)
/**
* атрибут объекта WebPass типа CK_ULONG - длина многоразового пароля - значение от 1 до JC_WP_MAX_PASSWORD_LENGTH
*/
#define CKA_WP_PASS_LENGTH          (CKA_VENDOR_DEFINED | 0x0009)
/**
* атрибут объекта WebPass  - начальное значение для алгоритма OTP - длина 20 байт для алгоритма JC_WP_OTP_ALGORITHM_SHA1 или 32 байта для других алгоритмов
*/
#define CKA_WP_OTP_SEED             (CKA_VENDOR_DEFINED | 0x000A)
/**
* атрибут объекта WebPass - значение многоразового пароля - не м.б. длиной больше JC_WP_MAX_PASSWORD_LENGTH
*/
#define CKA_WP_PASSWORD_VALUE       (CKA_VENDOR_DEFINED | 0x000B)
/**
* атрибут объекта WebPass - код платформы для URL тип CK_ULONG - значение - одно из JC_WP_URL_PLATFORM_CODE
*/
#define CKA_WP_URL_PLATFORM         (CKA_VENDOR_DEFINED | 0x000C)
/**
* атрибут объекта WebPass - значение URL - не м.б. длиной больше JC_WP_MAX_URL_LENGTH
*/
#define CKA_WP_URL                  (CKA_VENDOR_DEFINED | 0x000D)
/**
* атрибут объекта WebPass - Идентификатор объекта WebPass - типа JC_WP_ID
*/
#define CKA_WP_ID                   (CKA_VENDOR_DEFINED | 0x000E)
/**
* атрибут объекта профиля SecurLogon - имя пользователя - массив CK_UTF8_CHAR
*/
#define CKA_SL_USER_NAME            (CKA_VENDOR_DEFINED | 0x000F)
/**
* атрибут объекта профиля SecurLogon - имя домена - строка из CK_UTF8_CHAR
*/
#define CKA_SL_DOMAIN_NAME          (CKA_VENDOR_DEFINED | 0x0010)
/**
* атрибут объекта профиля SecurLogon - признак профиля по умолчанию - CK_BBOOL
*/
#define CKA_SL_DEFAULT              (CKA_VENDOR_DEFINED | 0x0011)
/**
* атрибут объекта профиля SecurLogon - тип пароля - JC_SL_PASSWORD_TYPE
*/
#define CKA_SL_PASSWORD_TYPE        (CKA_VENDOR_DEFINED | 0x0012)
/**
* атрибут объекта профиля SecurLogon - дата создания профиля - JC_SL_DATE
*/
#define CKA_SL_CREATED_DATE         (CKA_VENDOR_DEFINED | 0x0013)
/**
* атрибут объекта профиля SecurLogon - дата последнего изменения профиля - JC_SL_DATE
*/
#define CKA_SL_MODIFIED_DATE        (CKA_VENDOR_DEFINED | 0x0014)
/**
* атрибут объекта профиля SecurLogon - SID профиля - массив CK_BYTE
*/
#define CKA_SL_SID                  (CKA_VENDOR_DEFINED | 0x0015)
/**
* атрибут объекта профиля SecurLogon - признак присутствия пароля - CK_BBOOL
*/
#define CKA_SL_PASSWORD_EXISTS      (CKA_VENDOR_DEFINED | 0x0016)
/**
* атрибут объекта ключа - тип ключа - JC_F2_KEY_TYPE
*/
#define CKA_F2_KEY_TYPE             (CKA_VENDOR_DEFINED | 0x0017)
/**
* атрибут объекта ключа токена - идентификатор мастер-ключа - CK_OBJECT_HANDLE
*/
#define CKA_F2_MASTER_KEY_ID        (CKA_VENDOR_DEFINED | 0x0018)
/**
* атрибут объекта ключа токена - серийный номер токена, для которого создается ключ
*/
#define CKA_F2_TOKEN_SERIAL              (CKA_VENDOR_DEFINED | 0x0019)

/** Жизненный цикл объекта WebPass. */
typedef CK_ULONG JC_WP_LIFECYCLE;
/** объект WebPass не инициализирован. */
#define JC_WP_LIFECYCLE_EMPTY                       0x01
/** объект WebPass инициализирован. */
#define JC_WP_LIFECYCLE_INITIALIZED                 0x02
/** объект WebPass заблокирован. */
#define JC_WP_LIFECYCLE_LOCKED                      0x03

/** Тип объекта WebPass. */
typedef CK_ULONG JC_WP_TYPE;
/** объект WebPass типа одноразовый пароль (ОТР) . */
#define JC_WP_TYPE_OTP                         0x01
/** объект WebPass типа многоразовый пароль (PASS). */
#define JC_WP_TYPE_PASS                        0x02
/** объект WebPass типа ссылка (URL). */
#define JC_WP_TYPE_URL                         0x03

/** Алгоритм одноразового пароля. */
typedef CK_ULONG JC_WP_OTP_ALGORITHM;
/** RFC 4226 + HMAC-SHA1 (длина одноразового пароля – 6 символов). */
#define JC_WP_OTP_ALGORITHM_SHA1                    0x01
/** размер вектора инициализации в байтах для RFC 4226 + HMAC-SHA1 (длина одноразового пароля – 6 символов). */
#define JC_WP_OTP_ALGORITHM_SHA1_SEED_SIZE          20
/** RFC 4226 + HMAC-SHA256 (длина одноразового пароля – 6 символов). */
#define JC_WP_OTP_ALGORITHM_SHA256_6                0x02
/** размер вектора инициализации в байтах для RFC 4226 + HMAC-SHA256 (длина одноразового пароля – 6 символов). */
#define JC_WP_OTP_ALGORITHM_SHA256_6_SEED_SIZE      32
/** RFC 4226 + HMAC-SHA256 (длина одноразового пароля – 7 символов). */
#define JC_WP_OTP_ALGORITHM_SHA256_7                0x03
/** размер вектора инициализации в байтах для RFC 4226 + HMAC-SHA256 (длина одноразового пароля – 7 символов). */
#define JC_WP_OTP_ALGORITHM_SHA256_7_SEED_SIZE      32
/** RFC 4226 + HMAC-SHA256 (длина одноразового пароля – 8 символов). */
#define JC_WP_OTP_ALGORITHM_SHA256_8                0x04
/** размер вектора инициализации в байтах для RFC 4226 + HMAC-SHA256 (длина одноразового пароля – 8 символов). */
#define JC_WP_OTP_ALGORITHM_SHA256_8_SEED_SIZE      32

/** Максимальная длина названия объекта WebPass. */
#define JC_WP_MAX_NAME_LENGTH                       32

/** Максимальная длина префикса одноразового пароля. */
#define JC_WP_MAX_PREFIX_LENGTH                     32

/** Критерии качества многоразового пароля. */
typedef CK_ULONG JC_WP_PASS_QUALITY;
/** Использовать цифры в многоразовом пароле - только для генерируемого пароля. */
#define JC_WP_PASS_QUALITY_USE_DIGITS               0x01
/** Использовать английские буквы нижнего регистра в многоразовом пароле - только для генерируемого пароля. */
#define JC_WP_PASS_QUALITY_USE_LOWER_CASE           0x08
/** Использовать английские буквы верхнего регистра в многоразовом пароле - только для генерируемого пароля. */
#define JC_WP_PASS_QUALITY_USE_UPPER_CASE           0x10
/** Использовать спецсимволы в многоразовом пароле - только для генерируемого пароля. */
#define JC_WP_PASS_QUALITY_USE_SPECIAL              0x80
/** Добавлять код 0x0D в конец пароля */
#define JC_WP_PASS_QUALITY_ADD_CARRIAGE_RETURN      0x80000000
/** Все допустимые политики*/
#define JC_WP_PASS_QUALITY_ALL                      (JC_WP_PASS_QUALITY_USE_DIGITS | JC_WP_PASS_QUALITY_USE_LOWER_CASE | JC_WP_PASS_QUALITY_USE_UPPER_CASE | JC_WP_PASS_QUALITY_USE_SPECIAL | JC_WP_PASS_QUALITY_ADD_CARRIAGE_RETURN)

/** Максимальная длина одноразового пароля. */
#define JC_WP_MAX_PASSWORD_LENGTH                   160

typedef CK_ULONG JC_WP_URL_PLATFORM_CODE;
#define JC_WP_URL_PLATFORM_CODE_WINDOWS             0x02
#define JC_WP_URL_PLATFORM_CODE_MAC_OS              0x03
#define JC_WP_URL_PLATFORM_CODE_LINUX               0x04
#define JC_WP_URL_PLATFORM_CODE_SECRET_DISK         0x05
#define JC_WP_URL_PLATFORM_CODE_ANDROID_IOS         0x15

/** Максимальная длина URL. */
#define JC_WP_MAX_URL_LENGTH                        190

/** Идентификатор объекта WebPass */
typedef CK_ULONG JC_WP_ID;
#define JC_WP_ID_1                                  0x01
#define JC_WP_ID_2                                  0x02
#define JC_WP_ID_3                                  0x03

/** Тип пароля в профиле SecurLogon */
typedef CK_ULONG JC_SL_PASSWORD_TYPE;
#define JC_SL_PASSWORD_TYPE_MANUAL                  0x01
#define JC_SL_PASSWORD_TYPE_RANDOM                  0x02

/** Дата профиля */
typedef struct
{
    CK_ULONG year;
    CK_ULONG month;
    CK_ULONG day;
    CK_ULONG hour;
    CK_ULONG minutes;
    CK_ULONG seconds;
} JC_SL_DATE;
typedef JC_SL_DATE CK_PTR JC_SL_DATE_PTR;

/**
* идентификатор виртуального секретного ключа
*/
#define CT2_VIRTUAL_SECRET_KEY_ID "VSKO_ID"
/**
* размер идентификатора виртуального секретного ключа в байтах
*/
#define CT2_VIRTUAL_SECRET_KEY_ID_SIZE 7

/**
* идентификатор виртуального открытого ключа
*/
#define CT2_VIRTUAL_PUBLICK_KEY_ID "VPKO_ID"
/**
* размер идентификатора виртуального открытого ключа в байтах
*/
#define CT2_VIRTUAL_PUBLICK_KEY_ID_SIZE 7

#define CKR_CT2_PIN_ALREADY_SET         (CKR_VENDOR_DEFINED | 0x011)
/**
 * BIO уже инициализирована.
 */
#define CKR_BIO_ALREADY_INITIALIZED     (CKR_VENDOR_DEFINED | 0x012)
/**
* BIO не инициализирована.
*/
#define CKR_BIO_NOT_INITIALIZED         (CKR_VENDOR_DEFINED | 0x013)
/**
* Указанная библиотека не является библиотекой BIO
*/
#define CKR_BIO_NOT_BIO                 (CKR_VENDOR_DEFINED | 0x014)
/**
* Неправильный ПУК-код
*/
#define CKR_PUK_INCORRECT               (CKR_VENDOR_DEFINED | 0x015)
/**
* Невозможно разблокировать ПИН-код
*/
#define CKR_CANNOT_UNLOCK               (CKR_VENDOR_DEFINED | 0x016)
/**
* Слот заблокирован
*/
#define CKR_WEBPASS_SLOT_LOCKED         (CKR_VENDOR_DEFINED | 0x017)
/**
* Неправильный текущий режим работы Flash2
*/
#define CKR_INCORRECT_LIFE_CYCLE        (CKR_VENDOR_DEFINED | 0x018)
/**
* На устройстве Flash2 имеются не завершенные операции записи дынных
*/
#define CKR_DEVICE_BUSY                 (CKR_VENDOR_DEFINED | 0x019)
/**
* Устройство Flash2 уже инициализировано как токен пользователя или администратора
*/
#define CKR_TOKEN_ALREADY_INITIALIZED   (CKR_VENDOR_DEFINED | 0x020)
/**
* На устройстве Flash2 скрытые разделы уже монтированы
*/
#define CKR_ALREADY_MOUNTED             (CKR_VENDOR_DEFINED | 0x021)
/**
* На устройстве Flash2 разделы не размечены
*/
#define CKR_NO_PARTITIONS               (CKR_VENDOR_DEFINED | 0x022)
/**
* Устройство Flash2 приведено в некорректное состояние. Необходимо вызывать функцию JC_F2_RestoreState
*/
#define CKR_BROKEN_STATE                (CKR_VENDOR_DEFINED | 0x023)
/**
* Устройство Flash2 еще не инициализировано как токен пользователя или администратора
*/
#define CKR_TOKEN_NOT_INITIALIZED       (CKR_VENDOR_DEFINED | 0x024)
/**
* Для устройстве Flash2. Предъявлен неправильный ключ шифрования разделов
*/
#define CKR_INVALID_PARTITION_KEY       (CKR_VENDOR_DEFINED | 0x025)

/** префикс для собственных механизмов */
#define NSSCK_VENDOR_ALADDIN                0xC4900000 /** 0x80000000 | 0x44900000 */
/** специальный механизм для UEK */
#define CKM_UEK_DERIVE                      (NSSCK_VENDOR_ALADDIN | 0x001)
/** проверка сертификата - только для КТ2 */
#define CKM_VERIFY_CERIFICATE               (NSSCK_VENDOR_ALADDIN | 0x002)
/** ошибка TLS - необходимо передать больше данных для расшифровки сообщения */
#define CKR_NEED_MORE_DATA                  (NSSCK_VENDOR_ALADDIN | 0x003)

#define CKM_GOSTR3410_256_KEY_PAIR_GEN      (NSSCK_VENDOR_ALADDIN | 0x004)
#define CKM_GOSTR3410_256                   (NSSCK_VENDOR_ALADDIN | 0x005)
#define CKK_GOSTR3410_256                   (NSSCK_VENDOR_ALADDIN | 0x006)

typedef struct CK_UEK_DERIVE_PARAMS
{
    CK_BYTE_PTR pR;
    CK_ULONG ulRLen;
    CK_BYTE_PTR pHashParams;
    CK_ULONG ulHashParamsLen;
} CK_UEK_DERIVE_PARAMS;

typedef CK_UEK_DERIVE_PARAMS CK_PTR CK_UEK_DERIVE_PARAMS_PTR;


/**
* аутентификация по ПУК коду (только для Криптотокен-2)
*/
#define CKU_PUK             (0x80000001)
/**
* аутентификация по ответу на запрос внешней аутентификации(JC_CTRL_PKI_GET_CHALLENGE) (только для Laser)
*/
#define CKU_SO_RESPONSE     (0x80000002)
/**
* аутентификация администратором с установкой SecureMessaging (только для Криптотокен-2 и Flash2)
*/
#define CKU_SO_SM           (0x80000003)
/**
* аутентификация пользователем с установкой SecureMessaging (только для Криптотокен-2 и Flash2)
*/
#define CKU_USER_SM         (0x80000004)

/* специальные флаги инициализации */
#define CKF_DEVELOPER_MODE          0x80000000

#define CKF_DISABLE_CRYPTO_TOKEN    0x00000004
#define CKF_DISABLE_CRYPTO_TOKEN2   0x00000008
#define CKF_DISABLE_DATASTORE       0x00000010
#define CKF_DISABLE_LASER           0x00000020

#define CKF_SE_MODE                 0x00000040

/**
* специальный тип объекта для профилей SecurLogon
*/
#define CKO_SECUR_LOGON_PROFILE             (CKO_VENDOR_DEFINED | 0x00000001)
/**
* специальный тип объекта для слтов WebPass SecurLogon
*/
#define CKO_WEBPASS_OBJECT                  (CKO_VENDOR_DEFINED | 0x00000002)

/**
* информация о модели устройства
*/
typedef struct
{
    /**
    * модель
    */
    CK_UTF8CHAR model[32];
    /**
    * дата производства в формате ГГГГММДД ( 1 мая 2010 = 20100501 )
    */
    CK_BYTE manufacturingDate[8];
} JC_ISD_DATA;
typedef JC_ISD_DATA CK_PTR JC_ISD_DATA_PTR;

/**
* Тип аутентификации пользователя для токенов типа Laser
*/
typedef CK_ULONG JC_PKI_AUTHTYPE;
typedef JC_PKI_AUTHTYPE CK_PTR JC_PKI_AUTHTYPE_PTR;
/**
* неизвестен
*/
#define JC_PKI_AUTHTYPE_UNDEFINED   0x00
/**
* По ПИН-коду
*/
#define JC_PKI_AUTHTYPE_PIN         0x01
/**
* По отпечатку пальца
*/
#define JC_PKI_AUTHTYPE_BIO         0x03
/**
* По ПИН-коду или отпечатку пальца
*/
#define JC_PKI_AUTHTYPE_PIN_OR_BIO  0x04
/**
* По ПИН-коду и отпечатку пальца
*/
#define JC_PKI_AUTHTYPE_PIN_AND_BIO 0x05

/**
* Качество отпечатка
*/
typedef CK_ULONG JC_PKI_BIO_PURPOSE;
#define JC_BIO_PURPOSE_100     (0x7fffffff / 100)
#define JC_BIO_PURPOSE_1000    (0x7fffffff / 1000)
#define JC_BIO_PURPOSE_10000   (0x7fffffff / 10000)
#define JC_BIO_PURPOSE_100000  (0x7fffffff / 100000)
#define JC_BIO_PURPOSE_1000000 (0x7fffffff / 1000000)

/**
* Режим защищенного канала
*/
typedef CK_ULONG JC_PKI_SECURE_MESSAGING_MODE;
/**
* Защищенный канал выключен
*/
#define JC_PKI_SECURE_MESSAGING_MODE_OFF 0x00
/**
* Защищенный канал на ключевых парах по алгоритму RSA
*/
#define  JC_PKI_SECURE_MESSAGING_MODE_RSA 0x01
/**
* Защищенный канал на ключевых парах по алгоритму EC
*/
#define  JC_PKI_SECURE_MESSAGING_MODE_EC 0x02

/**
* настройки апплета PKI при инициализации
*/
typedef struct
{
    CK_BYTE IsPersonalized;

    CK_ULONG MaxDFSize;
    CK_BYTE UserMaxAttempts;
    CK_BYTE UserMaxUnblock;
    CK_BYTE AdminMaxAttempts;
    CK_BYTE AdminIsChalResp;
    JC_PKI_AUTHTYPE UserPinType;

    CK_BYTE CardType;

    CK_BYTE UserMinChars;
    CK_BYTE UserMaxChars;
    CK_BYTE UserMinAlphaChars;
    CK_BYTE UserMinLowerChars;
    CK_BYTE UserMinUpperChars;

    CK_BYTE UserMinDigits;
    CK_BYTE UserMinNonAlphaChars;
    CK_BYTE UserPinHistorySize;

    /* Параметры ПИН кода администратора. Неприменимо для 3des */
    CK_BYTE AdminMinChars;
    CK_BYTE AdminMaxChars;
    CK_BYTE AdminMinAlphaChars;
    CK_BYTE AdminMinLowerChars;
    CK_BYTE AdminMinUpperChars;

    CK_BYTE AdminMinDigits;
    CK_BYTE AdminMinNonAlphaChars;

    CK_ULONG UserPINValidForSeconds;
    CK_ULONG UserPINExpiresAfterDays;
    CK_BYTE AllowCardWipe;
    CK_BYTE BioImageQuality;
    JC_PKI_BIO_PURPOSE BioPurpose;
    CK_BYTE BioMaxFingers;

    CK_BYTE X931Use;
    CK_BYTE BioMaxUnblock;
    CK_BYTE UserMustChangeAfterUnlock;

    CK_BYTE UserMaxRepeatingPin;
    CK_BYTE UserMaxSequencePin;

    CK_BYTE AdminMaxRepeatingPin;
    CK_BYTE AdminMaxSequencePin;

    /* digital signature specific - not used in this version*/
    CK_BYTE DSSupport;
    CK_BYTE MaxRSA1024Keys;
    CK_BYTE MaxRSA2048Keys;

    CK_BYTE DSPinMaxChars;
    CK_BYTE DSPinMinChars;
    CK_BYTE DSPinMinDigits;
    CK_BYTE DSPinMinAlphaChars;
    CK_BYTE DSPinMinNonAlphaChars;

    CK_BYTE DSPinMinLowerChars;
    CK_BYTE DSPinMinUpperChars;
    CK_BYTE DSPinMinAlphabeticChars;
    CK_BYTE DSPinMaxRepeatingPin;
    CK_BYTE DSPinMaxSequencePin;
    CK_BYTE DSPinMaxUnblock;
    CK_BYTE DSPinMaxAttempts;

    CK_BYTE DSPUKMaxChars;
    CK_BYTE DSPUKMinChars;
    CK_BYTE DSPUKMinDigits;
    CK_BYTE DSPUKMinAlphaChars;
    CK_BYTE DSPUKMinNonAlphaChars;

    CK_BYTE DSPUKMinLowerChars;
    CK_BYTE DSPUKMinUpperChars;
    CK_BYTE DSPUKMinAlphabeticChars;
    CK_BYTE DSPUKMaxRepeatingPin;
    CK_BYTE DSPUKMaxSequencePin;
    CK_BYTE DSPUKMaxUnblock;
    CK_BYTE DSPUKMaxAttempts;

    CK_BYTE DSSynchronizationOption;
    CK_BYTE DSVerificationPolicy;
    CK_BYTE DSActivationPINValue[16];
    CK_BYTE DSActivationPINLen;
    CK_BYTE DSDeactivationPINValue[16];
    CK_BYTE DSDeactivationPINLen;

    CK_BYTE UserPinAlways;

    CK_BYTE BioType;

    CK_BYTE UserMustChangeAfterFirstUse;
    CK_BYTE StartDate[8];

    CK_BYTE DefaultFinger;

    /**
    * режим обмена между токеном и компьютером
    */
    JC_PKI_SECURE_MESSAGING_MODE SecureMessagingMode;
    /**
    * длина нового ПИН-кода администратора. м.б. = 0
    */
    CK_BYTE NewAdminPinLength;
    /**
    * буфер нового ПИН-кода администратора
    */
    CK_BYTE NewAdminPin[16];
} JC_PKI_PERSONALIZATION_INFO;
typedef JC_PKI_PERSONALIZATION_INFO CK_PTR JC_PKI_PERSONALIZATION_INFO_PTR;

/**
* информация о пине пользователя для Лазера
*/
typedef struct
{
    /**
    * Количество попыток ввода ПИН-кода пользователя
    */
    CK_BYTE UserPinRemains;
    /**
    * Количество попыток аутентификации пользователя через BIO
    */
    CK_BYTE UserBioPinRemains;
    /**
    * Количество попыток ввода ПИН-кода администратора
    */
    CK_BYTE AdminPinRemains;
} JC_PKI_PIN_INFO;
typedef JC_PKI_PIN_INFO CK_PTR JC_PKI_PIN_INFO_PTR;

/**
* тип поддерживаемой биометрии
*/
typedef CK_ULONG JC_PKI_BIO_BIOMETRY_TYPE;
#define JC_PKI_BIO_BIOMETRY_TYPE_PRECISE_BIOMATCH              0x81
#define JC_PKI_BIO_BIOMETRY_TYPE_PRECISE_ANSI                  0x82
#define JC_PKI_BIO_BIOMETRY_TYPE_NEUROTECHNOLOGY_MEGAMATCHER   0x83
#define JC_PKI_BIO_BIOMETRY_TYPE_PRECISE_ISO                   0x84
#define JC_PKI_BIO_BIOMETRY_TYPE_ID3_ISO                       0x85

/**
* информация о поддержке BIO
*/
typedef struct
{
    /**
    * Поддерживается ли биометрия
    */
    CK_BBOOL Enabled;
    /**
    * Тип аутентификации
    */
    JC_PKI_AUTHTYPE AuthType;
} JC_PKI_BIO_SUPPORT_INFO;
typedef JC_PKI_BIO_SUPPORT_INFO CK_PTR JC_PKI_BIO_SUPPORT_INFO_PTR;

/**
* состояние ПИН-кода для Криптотокен-2
*/
typedef struct
{
    /** признак установки PIN-кода */
    CK_BBOOL State;
    /** максимальное количество последовательно неправильно введенных ПИН-кодов */
    CK_ULONG MaxErrorCount;
    /** количество последовательно неправильно введенных ПИН-кодов */
    CK_ULONG ErrorCount;
} JC_CT2_PIN_STATE;
typedef JC_CT2_PIN_STATE CK_PTR JC_CT2_PIN_STATE_PTR;

/**
* политика ПИН-кода для Криптотокен-2
*/
typedef struct
{
    /** обязательное наличие в ПИН-коде прописных символов A-Z А-Я */
    CK_BBOOL UseUpperCaseLetters;
    /** обязательное наличие в ПИН-коде строчных символов a-z а-я */
    CK_BBOOL UseLowerCaseLetters;
    /** обязательное наличие в ПИН-коде цифр */
    CK_BBOOL UseDigits;
    /** обязательное наличие в ПИН-коде специальных символов находятся в диапазонах 20h–2Fh, 3Ah–40h, 5Bh–60h и 7Bh–7Eh */
    CK_BBOOL UseSpecial;
    /** признак необходимости смены ПИН-кода */
    CK_BBOOL PinMustBeChanged;
    /** приращение минимальной длины кода, т.е. минимальная для ПИН-кода = 6 + значение приращение. Значение приращения м.б. от 0 до 7 */
    CK_ULONG MinPinLengthAddition;
} JC_CT2_PIN_POLICY;
typedef JC_CT2_PIN_POLICY CK_PTR JC_CT2_PIN_POLICY_PTR;

typedef CK_ULONG JC_CT2_SECURE_MESSAGING_STATE;
/**
* Защищенный канал выключен
*/
#define JC_CT2_SECURE_MESSAGING_STATE_NONE 0
/**
* Защищенный канал в процессе установки
*/
#define JC_CT2_SECURE_MESSAGING_STATE_NOT_FINISHED 1
/**
* Защищенный канал включен
*/
#define JC_CT2_SECURE_MESSAGING_STATE_SIMPLE 2
/**
* Защищенный канал включен в эксклюзивном режиме
*/
#define JC_CT2_SECURE_MESSAGING_STATE_EXCLUSIVE 3

/**
* дополнительная информация о Криптотокен-2
*/
typedef struct
{
    /**
    * Главная версия прошивки
    */
    CK_BYTE Major;
    /**
    * Дополнительная версия прошивки
    */
    CK_BYTE Minor;
    /**
    * Номер выпуска прошивки
    */
    CK_BYTE Release;
    /**
    * количество разблокировок
    */
    CK_ULONG UnlockCount;
    /**
    * информация о персонализации
    */
    CK_BYTE PersonalizationInfo[64];
    /**
    * контрольная сумма
    */
    CK_BYTE CheckSum[32];
    /**
    * состояние PIN-кода пользователя
    */
    JC_CT2_PIN_STATE UserPINState;
    /**
    * состояние PIN-кода подписи
    */
    JC_CT2_PIN_STATE SignPINState;
    /**
    * состояние PUK-кода
    */
    JC_CT2_PIN_STATE PUKState;
    /**
    * Признак необходимости ввода ПИН-кода администратора в закрытом виде
    */
    CK_BBOOL SULoginThrowSM;
    /**
    * Признак разрешения пользователю менять PUK-код
    */
    CK_BBOOL UserPUKEnabled;
    /**
    * политика ПИН-кода пользователя
    */
    JC_CT2_PIN_POLICY UserPinPolicy;
    /**
    * политика ПИН-кода подписи
    */
    JC_CT2_PIN_POLICY UserSignPinPolicy;
    /**
    * состояние защищенного канала
    */
    JC_CT2_SECURE_MESSAGING_STATE SecureMessagingState;
} JC_CT2_EXTENDED_INFO;
typedef JC_CT2_EXTENDED_INFO CK_PTR JC_CT2_EXTENDED_INFO_PTR;

/**
* типы ПИН-кодов для КТ2
*/
typedef CK_ULONG JC_CT2_PIN_TYPE;
/**
* ПИН-код пользователя
*/
#define JC_CT2_PIN_TYPE_USER 1
/**
* ПИН-код подписи
*/
#define JC_CT2_PIN_TYPE_SIGNATURE 2

/**
* Свойства SWYX-считывателя
*/
typedef struct
{
    /** Тип экрана. 0 - только текст, 1 - графический. */
    CK_BYTE bDisplayType;
    /** Максимальное количество символов, помещающихся в одну строчку на экране. */
    CK_ULONG wLcdMaxCharacters;
    /** Максимальное количество строк. */
    CK_ULONG wLcdMaxLines;
    /** Ширина экрана в точках. */
    CK_ULONG wGraphicMaxWidth;
    /** Высота экрана в точках. */
    CK_ULONG wGraphicMaxHeight;
    /** Цветность экрана. 1 - черно-белый, 2 - градации серого (4 бита), 4 - цветной (4 бита) */
    CK_BYTE bGraphicColorDepth;
    /** Максимальное количество буквенных символов, одновременно помещающихся на экране. */
    CK_ULONG wMaxVirtualSize;
} SWYX_PROPERTIES_RESPONSE;
typedef SWYX_PROPERTIES_RESPONSE CK_PTR SWYX_PROPERTIES_RESPONSE_PTR;

typedef CK_ULONG JC_EX_X509_DATA_TYPE;
/** получить владельца сертификата. */
#define JC_EX_X509_DATA_TYPE_SUBJECT 1
/** получить издателя сертификата. */
#define JC_EX_X509_DATA_TYPE_ISSUER 2
/** получить серийный номер сертификата. */
#define JC_EX_X509_DATA_TYPE_SERIAL 3

/**
* тип апплета
*/
typedef CK_ULONG JC_APPLET_TYPE;
typedef JC_APPLET_TYPE CK_PTR JC_APPLET_TYPE_PTR;
#define JC_APPLET_TYPE_CRYPTO_TOKEN                 1
#define JC_APPLET_TYPE_CRYPTO_TOKEN_2               2
#define JC_APPLET_TYPE_LASER                        3
#define JC_APPLET_TYPE_DATA_STORE                   4
#define JC_APPLET_TYPE_FKH                          5
#define JC_APPLET_TYPE_PRO_JAVA                     6
#define JC_APPLET_TYPE_PRO                          7
#define JC_APPLET_TYPE_VASCO_CARDLESS               8
#define JC_APPLET_TYPE_WEBPASS                      9
#define JC_APPLET_TYPE_FLASH2                       10

/**
* Информация о считывателе
*/
typedef struct
{
    /**
    * массив атрибутов
    */
    CK_BYTE_PTR pAttr;
    /**
    * размер массива атрибутов в байтах
    */
    CK_ULONG AttrSize;
    /**
    * тег считывателя
    */
    CK_BYTE_PTR pJaCartaTag;
    /**
    * размер тега в байтах
    */
    CK_ULONG JaCartaTagSize;
    /**
    * серийный номер считывателя
    */
    CK_BYTE_PTR pSerialNumber;
    /**
    * размер серийного номера считывателя в байтах
    */
    CK_ULONG SerialNumberSize;
    /**
    * дата производства в секундах начиная с 01.01.1970
    */
    CK_ULONG ManufactureDate;
    /**
    * Количество апплетов
    */
    CK_ULONG AppletCount;
    /**
    * Апплеты
    */
    JC_APPLET_TYPE_PTR pApplets;
} JC_TOKEN_PROPERTIES;
typedef JC_TOKEN_PROPERTIES CK_PTR JC_TOKEN_PROPERTIES_PTR;

/** Не включать данные в сообщение PKCS#7 подписи. */
#define PKCS7_DETACHED_SIGNATURE        0x01
/** Вычислять значение хеш-функции на устройстве. */
#define PKCS7_HARDWARE_HASH             0x02
/** Выполнять проверку срока действия сертификата. */
#define PKCS7_CHECK_CERT_VALIDITY       0x04
/** Не выполнять хеширование, подписывать сразу данные. */
#define PKCS7_NOHASH                    0x08

/** Префикс для значений RDN, которые д.б. закодированы как ASN.1 NumericString. */
#define X509_NAME_NUMERICSTRING_PREFIX "NUMERICSTRING:"
/** Префикс для значений RDN, которые д.б. закодированы как ASN.1 UTF8String. */
#define X509_NAME_UTF8STRING_PREFIX "UTF8STRING:"

typedef CK_ULONG JC_CONFIRM_MODE;
/** Ввод без подтверждения */
#define JC_CONFIRM_MODE_ENTER_ONLY          1
/** Ввод с подтверждением */
#define JC_CONFIRM_MODE_ENTER_AND_REPEAT    2
/** Запрос только подтверждения */
#define JC_CONFIRM_MODE_REPEAT_ONLY         3

/** Счетчики ПИН-кодов для КТ1 */
typedef struct
{
    /** Максимально допустимое количество ошибок ввод ПИН-кода. */
    CK_ULONG MaxPinErrors;
    /** Текущее количество ошибок ввод ПИН-кода пользователя. */
    CK_ULONG UserPinErrors;
    /** Текущее количество ошибок ввод ПИН-кода администратора. */
    CK_ULONG AdminPinErrors;
} JC_CT1_PIN_COUNTERS;
typedef JC_CT1_PIN_COUNTERS CK_PTR JC_CT1_PIN_COUNTERS_PTR;

/** Счетчики ПИН-кодов для DataStore */
typedef struct
{
    /** Максимально допустимое количество ошибок ввод ПИН-кода пользователя. */
    CK_ULONG UserMaxErrors;
    /** Текущее количество ошибок ввод ПИН-кода пользователя. */
    CK_ULONG UserPinErrors;
    /** Максимально допустимое количество ошибок ввод ПИН-кода администратора. */
    CK_ULONG AdminMaxErrors;
    /** Текущее количество ошибок ввод ПИН-кода администратора. */
    CK_ULONG AdminPinErrors;
} JC_DS_PIN_COUNTERS;
typedef JC_DS_PIN_COUNTERS CK_PTR JC_DS_PIN_COUNTERS_PTR;


/** Режим работы WebPass. */
typedef CK_ULONG JC_WP_MODE;
#define JC_WEBPASS_MODE_CCID        0x01
#define JC_WEBPASS_MODE_HID         0x02
#define JC_WEBPASS_MODE_HID_CCID    0x03

/** Дополнительная информация о WebPass */
typedef struct
{
    /** Режим работы. */
    JC_WP_MODE Mode;
    /** Код последней ошибки. */
    CK_ULONG LastErrorCode;
    /** Количество нажатий на кнопку. */
    CK_ULONG ButtonClickCount;
    /** Количество подключений к USB. */
    CK_ULONG USBConnectionCount;
} JC_WP_INFO;
typedef JC_WP_INFO CK_PTR JC_WP_INFO_PTR;

/** Информация о WebPass, заданная на производстве  */
typedef struct
{
    /** Версия прошивки major, minor*/
    CK_VERSION FirmwareVersionHigh;
    /** Версия прошивки build, release*/
    CK_VERSION FirmwareVersionLow;
    /** Дата производства */
    CK_DATE Date;
    /** Название модели*/
    CK_UTF8CHAR ModelName[32];
    /** Серийный номер */
    CK_UTF8CHAR SerialNumber[16];
} JC_WP_PRODUCTION_INFO;
typedef JC_WP_PRODUCTION_INFO CK_PTR JC_WP_PRODUCTION_INFO_PTR;

typedef CK_ULONG JC_LOG_MODE;
#define JC_LOG_MODE_OFF             0
#define JC_LOG_MODE_DEBUG           3
#define JC_LOG_MODE_DEBUG_APDU      4

/** Минимальный размер раздела в байтах */
#define JC_F2_MIN_PARTITION_SIZE 10485760

/** Жизненные циклы Flash2 */
typedef CK_ULONG JC_F2_LIFE_CYCLE;
/** не инициализирован */
#define JC_F2_LIFE_CYCLE_EMPTY              0x01
/** инициализирован */
#define JC_F2_LIFE_CYCLE_INITIALIZED        0x02
/** рабочее состояние */
#define JC_F2_LIFE_CYCLE_READY              0x03
/** скрытый раздел подключен */
#define JC_F2_LIFE_CYCLE_READY_MOUNTED      0x04

/**
* политика ПИН-кода для Flash2
*/
typedef struct
{
    /** обязательное наличие в ПИН-коде прописных символов A-Z А-Я */
    CK_BBOOL UseUpperCaseLetters;
    /** обязательное наличие в ПИН-коде строчных символов a-z а-я */
    CK_BBOOL UseLowerCaseLetters;
    /** обязательное наличие в ПИН-коде цифр */
    CK_BBOOL UseDigits;
    /** обязательное наличие в ПИН-коде специальных символов находятся в диапазонах 20h–2Fh, 3Ah–40h, 5Bh–60h и 7Bh–7Eh */
    CK_BBOOL UseSpecial;
    /** признак необходимости смены ПИН-кода */
    CK_BBOOL PinMustBeChanged;
    /** приращение минимальной длины кода, т.е. минимальная для ПИН-кода = 6 + значение приращение. Значение приращения м.б. от 0 до 7 */
    CK_ULONG MinPinLengthAddition;
} JC_F2_PIN_POLICY;
typedef JC_F2_PIN_POLICY CK_PTR JC_F2_PIN_POLICY_PTR;

/**
* состояние ПИН-кода для Flash2
*/
typedef struct
{
    /** признак установки PIN-кода */
    CK_BBOOL State;
    /** максимальное количество последовательно неправильно введенных ПИН-кодов */
    CK_ULONG MaxErrorCount;
    /** количество последовательно неправильно введенных ПИН-кодов */
    CK_ULONG ErrorCount;
} JC_F2_PIN_STATE;
typedef JC_F2_PIN_STATE CK_PTR JC_F2_PIN_STATE_PTR;

/** типы алгоритмов шифрования скрытых разделов*/
typedef CK_ULONG JC_F2_ALGORITHM_TYPE;
#define JC_F2_ALGORITHM_TYPE_GOST28147          1
#define JC_F2_ALGORITHM_TYPE_GOST28147_FAST     2

/** Информация о Flash2 */
typedef struct
{
    /** версия прошивки */
    CK_ULONG Version;
    /** Количество доступной памяти */
    CK_ULONG TotalMemory;
    /** Размер открытого RW раздела. */
    CK_ULONG PublicRWSize;
    /** Размер открытого CD-ROM раздела */
    CK_ULONG PublicCDSize;
    /** Размер скрытого CD-ROM раздела */
    CK_ULONG PrivateCDSize;
    /** Размер ISO-образа, записанного в открытый CD-ROM раздел */
    CK_ULONG PublicISOSize;
    /** Размер ISO-образа, записанного в скрытый CD-ROM раздел */
    CK_ULONG PrivateISOSize;
    /** Жизненный цикл */
    JC_F2_LIFE_CYCLE LifeCycle;
    /** Общее количество успешных подключений скрытого раздела в доверенной среде */
    CK_ULONG SuccessMountCount;
    /** Общее количество неуспешных подключений скрытого раздела в доверенной среде */
    CK_ULONG ErrorMountCount;
    /** Количество успешных отключений скрытого раздела */
    CK_ULONG SuccessUmountCount;
    /** Число выполненных команд "Очистить карту памяти" */
    CK_ULONG ClearCount;
    /** количество разблокировок */
    CK_ULONG UnlockCount;
    /** состояние PIN-кода пользователя */
    JC_F2_PIN_STATE UserPINState;
    /** состояние PUK-кода */
    JC_F2_PIN_STATE PUKState;
    /** политика ПИН-кода пользователя */
    JC_F2_PIN_POLICY UserPinPolicy;
    /** серийный номер токена для генерации ключа токена */
    CK_BYTE SerialNumber[16];
    /** алгоритм шифрования скрытых разделов. Если не определен, то равен CK_UNAVAILABLE_INFORMATION */
    JC_F2_ALGORITHM_TYPE EncryptionAlgorithm;
    /** признак наличия ключа шифрования скрытых разделов */
    CK_BBOOL HasPartitionKey;
} JC_F2_EXTENDED_INFO;
typedef JC_F2_EXTENDED_INFO CK_PTR JC_F2_EXTENDED_INFO_PTR;

/** Типы ключей */
typedef CK_ULONG JC_F2_KEY_TYPE;
#define JC_F2_KEY_TYPE_SO           1
#define JC_F2_KEY_TYPE_USER         2
#define JC_F2_KEY_TYPE_S            3

/**
* типы ПИН-кодов для Flash2
*/
typedef CK_ULONG JC_F2_PIN_TYPE;
/** ПИН-код пользователя */
#define JC_F2_PIN_TYPE_USER 1

/** типы токенов */
typedef CK_ULONG JC_F2_TOKEN_TYPE;
typedef JC_F2_TOKEN_TYPE CK_PTR JC_F2_TOKEN_TYPE_PTR;

#define JC_F2_TOKEN_TYPE_SO      1
#define JC_F2_TOKEN_TYPE_USER    2

/** параметры для вычисления ответа на запрос подключения скрытых разделов */
typedef struct
{
    CK_BYTE_PTR pMasterKey;
    CK_ULONG ulMasterKeySize;
    CK_BYTE_PTR pAuthorizationKey;
    CK_ULONG ulAuthorizationKeySize;
    CK_BYTE_PTR pChallenge;
    CK_ULONG ulChallengeSize;
} JC_F2_MOUNT_CHALLENGE_INFO;
typedef JC_F2_MOUNT_CHALLENGE_INFO CK_PTR JC_F2_MOUNT_CHALLENGE_INFO_PTR;

/** данные для инициализации токена пользователя */
typedef struct
{
    CK_BYTE AuthorizationKey[32];
    CK_BYTE PartitionKey[32];
} JC_F2_INIT_RESPONSE;
typedef JC_F2_INIT_RESPONSE CK_PTR JC_F2_INIT_RESPONSE_PTR;

#define JC_MIN_AUTHORIZATION_KEY_INDEX      0x00
#define JC_MAX_AUTHORIZATION_KEY_INDEX      0x0F

#endif /* JC_PKCS11_TYPES_H */
