#include <string>
#include <vector>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <exception>
#include <boost/program_options.hpp>

#include <openssl/bio.h>
#include <openssl/x509.h>
#include <openssl/pem.h>

#include "P11Loader.h"
#include "Utils.h"
#include "tool.h"

namespace po = boost::program_options;
using namespace std;

int main(int argc, char** argv) {
    bool initToken = false;
    bool createCSR = false;
    bool createKeys = false;
    bool listPublicKeys = false;
    bool listPrivateKeys = false;
    bool listCerts = false;
    bool login = false;
    bool storeCertificate = false;

    po::options_description desc("Allowed options");
    desc.add_options()
        ("help,h", "produce help message")
        ("input-file,f", po::value<string>(), "input file")
        ("output-file,o", po::value<string>(), "output file")
        ("init-token,I",
            po::bool_switch(&initToken)->default_value(false), "initialize token")
        ("user-pin", po::value<string>()->default_value("1234567890"), "Initialize PIN")

        ("create-csr,C",
            po::bool_switch(&createCSR)->default_value(false), "create CSR")
        ("dn", po::value< vector<string> >(), "distinguished name")
        ("attr", po::value< vector<string> >(), "attribute")
        ("ext", po::value< vector<string> >(), "extension")

        ("create-keys,K",
            po::bool_switch(&createKeys)->default_value(false), "generate key pair")
        ("id", po::value<string>(), "id")
        ("label", po::value<string>(), "label")

        ("list-certs,L",
            po::bool_switch(&listCerts)->default_value(false), "list certificates")
        ("list-public-keys", po::bool_switch(&listPublicKeys)->default_value(false), "list public keys")
        ("list-private-keys", po::bool_switch(&listPrivateKeys)->default_value(false), "list private keys")

        ("login", po::bool_switch(&login)->default_value(false), "login")

        ("delete-cert,D",
            po::value<int>()->default_value(0), "delete certificate by handle")

        ("store-certificate,S",
            po::bool_switch(&storeCertificate)->default_value(false),
            "store certificate to token")

        ("pin,P", po::value<string>()->default_value(""), "PIN")
        ("module,M",
            po::value<string>()->default_value("/usr/lib64/pkcs11/libjcpkcs11-2.so"),
            "path to the PKCS11 library");
    
    try {
        po::variables_map vm;
        
        po::store(po::command_line_parser(argc, argv).
                options(desc).run(), vm);
        po::notify(vm);

        if (vm.count("help")) {
            cout << desc << endl;
            return 0;
        }

        auto deleteCert = (CK_OBJECT_HANDLE)vm["delete-cert"].as<int>();
        string pin = vm["pin"].as<string>();

        if (initToken) {
            JacartaTool jt(vm["module"].as<string>().c_str());
            jt.initialize();
            auto slot = jt.findGOSTSlot();
            jt.initToken(slot, pin);  // here pin is admin pin

            jt.openSession(slot);
            jt.login(pin, true);
            jt.initPIN(vm["user-pin"].as<string>());
            return 0;
        }

        if (login && pin == "") {
            cerr << "PIN is required to login" << endl;
            return 1;
        }

        if (listCerts) {
            JacartaTool jt(vm["module"].as<string>().c_str());
            jt.initialize();
            auto slot = jt.findGOSTSlot();
            jt.openSession(slot);
            if (login) {
                jt.login(pin);
            }
            auto certs = jt.findCertificates();
            for (const auto& cert: certs) {
                cout << "Certificate " << cert << ":" << endl;
                cout << jt.getCertificateInfo(cert) << endl;
            }
            return 0;
        }

        if (listPublicKeys || listPrivateKeys) {
            JacartaTool jt(vm["module"].as<string>().c_str());
            jt.initialize();
            auto slot = jt.findGOSTSlot();
            jt.openSession(slot);
            if (login) {
                jt.login(pin);
            }

            
            auto keys = listPublicKeys? jt.findPublicKeys() : jt.findPrivateKeys();
            for (const auto& key: keys) {
                cout << "Key " << key << ":" << endl;
                cout << jt.getKeyInfo(key) << endl;
            }
            return 0;
        }

        if (deleteCert) {
            JacartaTool jt(vm["module"].as<string>().c_str());
            jt.initialize();
            auto slot = jt.findGOSTSlot();
            jt.openSession(slot);
            if (login) {
                jt.login(pin);
            }
            // get certificate descriptors
            jt.findCertificates();
            jt.deleteCertificate(deleteCert);
            return 0;
        }

        if (createCSR) {
            string usage = "Usage:\n\njacarta-tool --create-csr -o <FILE> --dn <DN> \\\n\t[--ext <EXT> --attr <ATTR>] --login --pin <PIN>";
            if (!vm.count("output-file")) {
                cerr << "Output file not specified\n" << usage << endl;
                return 1;
            }
            if (!vm.count("dn")) {
                cerr << "At least 1 --dn argument should be present\n" << usage << endl;
                return 1;
            }
            if (!login) {
                cerr << "Login required" << endl;
                return 1;
            }
            JacartaTool jt(vm["module"].as<string>().c_str());
            jt.initialize();
            auto slot = jt.findGOSTSlot();
            jt.openSession(slot);
            if (login) {
                jt.login(pin);
            }
            auto keys = jt.findPublicKeys();         
            CK_OBJECT_HANDLE publicKey;
            if (keys.size() > 1) {
                for (const auto& key: keys) {
                    cout << "Found public key:\n";
                    cout << jt.getKeyInfo(key) << endl;
                }

                cout << "Select the key: ";
                cin >> publicKey;
            } else if (keys.size() == 1) {
                publicKey = keys.at(0);
            } else if (keys.size() == 0) {
                throw runtime_error("No keys found, please, generate a key pair");
            }
            
            auto dns = vm["dn"].as< vector<string> >();
            auto attrs = vm.count("attr")? vm["attr"].as<vector<string> >() : vector<string>();
            auto exts = vm.count("ext")? vm["ext"].as< vector<string> >() : vector<string>();
            string csrFile = vm["output-file"].as<string>();
            
            jt.createCSR(csrFile, publicKey, dns, attrs, exts);
            return 0;
        }

        if (createKeys) {
            JacartaTool jt(vm["module"].as<string>().c_str());
            jt.initialize();
            auto slot = jt.findGOSTSlot();
            jt.openSession(slot);
            if (login) {
                jt.login(pin);
            }
            string usage = "Usage:\n\njacarta-tool --create-keys -id <ID> --label <LABEL>";
            if (!vm.count("id")) {
                cerr << "key id not specified\n" << usage << endl;
                return 1;
            }
            if (!vm.count("label")) {
                cerr << "key label not specified\n" << usage << endl;
                return 1;
            }
            const string id = vm["id"].as<string>();
            const string label = vm["label"].as<string>();

            jt.generateGOSTR3410KeyPair(id, label);
            return 0;
        }

        if (storeCertificate) {
            JacartaTool jt(vm["module"].as<string>().c_str());
            jt.initialize();
            auto slot = jt.findGOSTSlot();
            jt.openSession(slot);
            if (login) {
                jt.login(pin);
            }
            string usage = "jacarta-tool --store-certificate -f <FILE> --label <LABEL>";
            if (!vm.count("input-file")) {
                cerr << "DER-encoded X.509 certificate file not specified\n" << usage << endl;
                return 1;
            }
            if (!vm.count("label")) {
                cerr << "certificate label not specified\n" << usage << endl;
                return 1;
            }
            const string label = vm["label"].as<string>();
            const string certfile = vm["input-file"].as<string>();

            jt.storeCertificate(label, certfile);
            return 0;
        }

    } catch (const exception& e) {
        cerr << "Error: " << e.what() << endl;
        return 1;
    }

    return 0;
}