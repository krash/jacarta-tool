#include <stdexcept>
#include <exception>
#include <sstream>
#include <iomanip>
#include <memory>
#include <vector>
#include <openssl/bio.h>
#include <openssl/x509.h>
#include <openssl/pem.h>

#include "tool.h"
#include "Utils.h"


std::vector<CK_UTF8CHAR_PTR> parse_kwargs(const std::vector<std::string>& pairs);
void check_res(CK_RV rv, const char *message);


JacartaTool::JacartaTool(const char *module):
    _session(0), _nslots(0)
{
    _p11 = std::make_unique<P11Loader>(module);
    if (!_p11->IsInitialized()) {
        throw std::runtime_error("Can't initialize PKCS11 loader");
    }
}

JacartaTool::~JacartaTool() {
    _p11->functions()->C_Logout(_session);
    _p11->functions()->C_CloseSession(_session);
}

void JacartaTool::initialize() {
    auto rv = _p11->functions()->C_Initialize(0);
    check_res(rv, "C_Initialize");

    // получаем список слотов; в стандарте pkcs11 под cписком слотов
    // понимается список апплетов на всех доступных смарткартах
    rv = _p11->functions()->C_GetSlotList(CK_TRUE, NULL, &_nslots);
	check_res(rv, "C_GetSlotList");

    if (_nslots == 0) {
        throw std::runtime_error("No tokens are present");
    }
}

void JacartaTool::initToken(CK_SLOT_ID slot, const string& pin) {
    CK_BYTE label[32];
    memset(label, 0, sizeof(label));
    memcpy(label, "JaCarta", strlen("JaCarta"));

    auto rv = _p11->functions()->C_InitToken(
        slot, (CK_CHAR_PTR)pin.c_str(), strlen(pin.c_str()), label);
    check_res(rv, "C_InitToken");
}

void JacartaTool::initPIN(const std::string& pin) {
    auto rv = _p11->functions()->C_InitPIN(
        _session, (CK_CHAR_PTR) pin.c_str(), strlen(pin.c_str()));
    check_res(rv, "C_InitPIN");
}

CK_SLOT_ID JacartaTool::findGOSTSlot() {
    auto slots = std::make_unique<CK_SLOT_ID_PTR>(new CK_SLOT_ID[_nslots]);
    auto rv = _p11->functions()->C_GetSlotList(CK_TRUE, *slots.get(), &_nslots);
    check_res(rv, "C_GetSlotList");

    auto gostFound = false;
    CK_SLOT_ID gostSlotId = 0;
    // поиск апплета ГОСТ
	for (CK_ULONG i = 0; i < _nslots; ++i) {
		// получение информации о токене в слоте; в стандарте pkcs11 под токеном
        // понимется апплет на смарткарте
        CK_TOKEN_INFO tokenInfo;
        auto slot = *slots.get()[i];
		rv = _p11->functions()->C_GetTokenInfo(slot, &tokenInfo);
		check_res(rv, "C_GetTokenInfo");

		const char* str = strstr((const char*)tokenInfo.model, JC_MODEL_CRYPTOTOKEN_1);
		if (str) {
            gostFound = true;
			gostSlotId = slot;

			// std::cout << "Applet GOST found in slot " << (unsigned int)nGostSlotId << std::endl;
			break;
		}
	}

	if (!gostFound) {
		throw std::runtime_error("No smartcard with GOST applet found");
	}

    return gostSlotId;
}

void JacartaTool::openSession(CK_SLOT_ID slot) {
    if (!_session) {
        closeSession();
    }

    auto rv = _p11->functions()->C_OpenSession(
        slot, (CKF_SERIAL_SESSION | CKF_RW_SESSION), 0, 0, &_session);
    check_res(rv, "C_OpenSession");
}

void JacartaTool::closeSession() {
    _p11->functions()->C_CloseSession(_session);
}

void JacartaTool::login(const std::string& pin, bool admin) {
    auto loginType = admin? CKU_SO : CKU_USER;
    auto rv = _p11->functions()->C_Login(
        _session,
        loginType,
        (CK_CHAR_PTR)pin.c_str(),
        strlen(pin.c_str())
    );
	check_res(rv, "C_Login");
}

void JacartaTool::logout() {
    _p11->functions()->C_Logout(_session);
}

std::vector<CK_OBJECT_HANDLE> JacartaTool::findCertificates() {
    CK_ULONG certClass = CKO_CERTIFICATE;	
	CK_ATTRIBUTE attrs[] = {
		{CKA_CLASS, &certClass, sizeof(certClass)},
	};

    auto nattrs = sizeof(attrs) / sizeof(CK_ATTRIBUTE);
    auto rv = _p11->functions()->C_FindObjectsInit(_session, attrs, nattrs);
	check_res(rv, "C_FindObjectsInit");

    // ищем сертификаты
    CK_OBJECT_HANDLE certs[128];
    CK_ULONG ncerts = 0;
	rv = _p11->functions()->C_FindObjects(
        _session,
        certs,
        sizeof(certs) / sizeof(CK_OBJECT_HANDLE),
        &ncerts
    );
	check_res(rv, "C_FindObjects");

	rv = _p11->functions()->C_FindObjectsFinal(_session);
	check_res(rv, "C_FindObjectsFinal");

    std::vector<CK_OBJECT_HANDLE> handles;
    for (CK_ULONG i=0; i < ncerts; i++) {
        handles.push_back(certs[i]);
    }
    return handles;
}

std::string JacartaTool::getCertificateInfo(CK_OBJECT_HANDLE cert) {
	CK_CHAR_PTR pCertInfo = NULL;		// информация о сертификате
	CK_ULONG ulCertInfoLength = 0;	// длина информации о сертификате

	auto rv = _p11->exFunctions()->getCertificateInfo(_session, cert, &pCertInfo, &ulCertInfoLength);
	check_res(rv, "getCertificateInfo");

    std::string info((char*)pCertInfo, ulCertInfoLength);
	_p11->exFunctions()->freeBuffer(pCertInfo);
    return info;
}

std::string JacartaTool::getKeyInfo(CK_OBJECT_HANDLE key) {
    CK_RV rv;
	CK_UTF8CHAR *label = (CK_UTF8CHAR *)malloc(80);
	CK_BYTE *id = (CK_BYTE *) malloc(10);
	size_t label_len;
	char *label_str;
    std::stringstream sstr;

	memset(id, 0, 10);

	CK_ATTRIBUTE tpl[] = {
		{CKA_LABEL, label, 80},
		{CKA_ID, id, 1}
	};

	rv = _p11->functions()->C_GetAttributeValue(_session, key, tpl, 2);
	check_res(rv, "GetAttributeValue");

	label_len = tpl[0].ulValueLen;
	if (label_len > 0) {
		label_str = (char*)malloc(label_len + 1);
		memcpy(label_str, label, label_len);
		label_str[label_len] = '\0';
		sstr << "\tKey label: " << label_str << "\n";
		free(label_str);
	} else {
		sstr << "\tKey label too large, or not found\n";
	}
	if (tpl[1].ulValueLen > 0) {
		sstr << "\tKey ID: " << std::hex << std::setfill('0') << std::setw(2) << id[0] << "\n";
	} else {
		sstr << "\tKey id too large, or not found\n";
	}

	free(label);
	free(id);

    return sstr.str();
}

void JacartaTool::deleteCertificate(CK_OBJECT_HANDLE cert) {
    // TODO check if cerificate is really there
    auto rv = _p11->functions()->C_DestroyObject(_session, cert);
	check_res(rv, "C_DestroyObject");
}

std::vector<CK_OBJECT_HANDLE> JacartaTool::findPrivateKeys() {
    return this->findKeys(false);
}

std::vector<CK_OBJECT_HANDLE> JacartaTool::findPublicKeys() {
    return this->findKeys(true);
}

std::vector<CK_OBJECT_HANDLE> JacartaTool::findKeys(bool pub) {
    CK_ULONG objectCount;
    CK_ULONG keyClass = CKO_PUBLIC_KEY;
    if (!pub) {
        keyClass = CKO_PRIVATE_KEY;
    }
    CK_BBOOL bTrue = CK_TRUE;
    CK_OBJECT_HANDLE hKey;

    CK_ATTRIBUTE pubKeySearchAttribs[] =
    {
        { CKA_CLASS, &keyClass, sizeof(keyClass) },
        { CKA_TOKEN, &bTrue, sizeof(bTrue) },
    };
    auto rv = _p11->functions()->C_FindObjectsInit(
        _session,
        pubKeySearchAttribs,
        sizeof(pubKeySearchAttribs) / sizeof(CK_ATTRIBUTE)
    );
    check_res(rv, "C_FindObjectsInit");

    // int totalKeys = 0;
    objectCount = 0;
    std::vector<CK_OBJECT_HANDLE> handles;
    do {
        rv = _p11->functions()->C_FindObjects(_session, &hKey, 1, &objectCount);
        check_res(rv, "C_FindObjects");

        if (objectCount > 0) {
            // totalKeys++;
            handles.push_back(hKey);
            // show_key_info(loader, _session, hKey);
        }
    } while(objectCount > 0);

    rv = _p11->functions()->C_FindObjectsFinal(_session);
    check_res(rv, "C_FindObjectsFinal");

    return handles;
}

void JacartaTool::createCSR(const std::string& outputFile,
                                CK_OBJECT_HANDLE publicKey,
                                const std::vector<std::string>& dns,
                                const std::vector<std::string>& attrs,
                                const std::vector<std::string>& exts) {
    auto dnsPtrs = parse_kwargs(dns);
    auto dnBuf = dnsPtrs.data();

    auto attrsPtrs = parse_kwargs(attrs);
    auto attrsBuf = attrs.size()? attrsPtrs.data() : NULL;

    auto extsPtrs = parse_kwargs(exts);
    auto extsBuf = extsPtrs.data();

	CK_ULONG ulCsrLength = 0;		// длина запроса на сертификат

    CK_BYTE_PTR pCsr = NULL;
    // генерируем запрос на сертификат
    auto rv = _p11->exFunctions()->createCSR(
        _session,
        publicKey,
        dnBuf,
        dns.size()*2,
        &pCsr,
        &ulCsrLength,
        0,						// private key handle
        attrsBuf,					// attributes array
        attrs.size()*2,		// number of strings in attributes array
        extsBuf,
        exts.size()*2
    );
    check_res(rv, "createCSR");

    FILE* fpCsr = NULL;
    fpCsr = fopen(outputFile.c_str(), "wb");
    if(fpCsr == NULL) {
        throw std::runtime_error("Failed to open CSR file");
    }

    fwrite(pCsr, 1, ulCsrLength, fpCsr);
    fclose(fpCsr);
}

void JacartaTool::storeCertificate(const string& label,
                                   const string& file) {
    FILE* fp = fopen(file.c_str(), "rb");
    if (fp == NULL) {
        throw std::runtime_error("Failed to open certificate file");
    }

    // get certificate file size
    CK_ULONG certBufLen = 0;
    fseek(fp, 0, SEEK_END);
    certBufLen = ftell(fp);
    fseek(fp, 0, SEEK_SET);

    // load certificate from file
    X509* x509 = NULL;
    // auto certBuf = std::make_unique<CK_BYTE_PTR>(new CK_BYTE[certBufLen]);
    auto certBuf = std::make_unique<unsigned char*>(new unsigned char[certBufLen]);
    const unsigned char* pCertBuf = *certBuf.get();
    fread(*certBuf.get(), 1, certBufLen, fp);
    fclose(fp);

    if (!d2i_X509(&x509, &pCertBuf, certBufLen)) {
        throw std::runtime_error("Unable to decode a buffer");
    }

    auto serialNumber = X509_get_serialNumber(x509);
    auto subjectName = X509_get_subject_name(x509);

    unsigned char* subjectBuf = NULL;
    int subjectBufLen;

    subjectBufLen = i2d_X509_NAME(subjectName, &subjectBuf);
    if (subjectBufLen < 0) {
        throw std::runtime_error("Unable to encode subject name");
    }

    CK_ULONG nCertClass = CKO_CERTIFICATE;
    CK_CERTIFICATE_TYPE nCertificateType = CKC_X_509;
    CK_BBOOL bToken = TRUE;

    CK_ATTRIBUTE attrs[] = {
        { CKA_CLASS, (CK_VOID_PTR)&nCertClass, sizeof(nCertClass) },
        { CKA_TOKEN, (CK_VOID_PTR)&bToken, sizeof(bToken) },
        { CKA_LABEL, (CK_VOID_PTR)label.c_str(), (CK_ULONG)strlen(label.c_str()) },
        { CKA_SERIAL_NUMBER, (CK_VOID_PTR)serialNumber->data, (CK_ULONG)serialNumber->length },
        { CKA_SUBJECT, (CK_VOID_PTR)subjectBuf, (CK_ULONG)subjectBufLen },
        { CKA_CERTIFICATE_TYPE, (CK_VOID_PTR)&nCertificateType, sizeof(nCertificateType) },
        { CKA_VALUE, (CK_VOID_PTR)*certBuf.get(), certBufLen }
    };

    // store certificate on the token
    CK_OBJECT_HANDLE hCert;
    auto rv = _p11->functions()->C_CreateObject(
        _session, attrs, sizeof(attrs) / sizeof(CK_ATTRIBUTE), &hCert);
    check_res(rv, "C_CreateObject");
}

void JacartaTool::generateGOSTR3410KeyPair(const string& id,
                                           const string& label) {
    string pubKeyLabel = label;
    string privKeyLabel = label + " private key";
    
    CK_BBOOL			bToken = TRUE;
	CK_BBOOL			bTrue = CK_TRUE;
	CK_BBOOL			bFalse = CK_FALSE;
    
    CK_MECHANISM mech = { CKM_GOSTR3410_KEY_PAIR_GEN, NULL_PTR, 0 };
    CK_BYTE STR_CRYPTO_PRO_A[] = { 0x06, 0x07, 0x2A, 0x85, 0x03, 0x02, 0x02, 0x23, 0x01 };
    CK_BYTE STR_CRYPTO_PRO_GOST3411[] = { 0x06, 0x07, 0x2A, 0x85, 0x03, 0x02, 0x02, 0x1E, 0x01 };
    
	CK_ATTRIBUTE privKeyAttribs[] = 
	{
		{CKA_TOKEN, (CK_VOID_PTR) &bToken, sizeof(bToken)},
		{CKA_LABEL, (CK_VOID_PTR) privKeyLabel.c_str(), (CK_ULONG) strlen(privKeyLabel.c_str())},
		{CKA_GOSTR3410_PARAMS, (CK_VOID_PTR) STR_CRYPTO_PRO_A, sizeof(STR_CRYPTO_PRO_A)},			// тип ключевой пары согласно RFC 4357
		{CKA_PRIVATE, &bTrue, sizeof(bTrue) },														// не виден без ввода пин кода пользователя
		{CKA_ID, (CK_VOID_PTR) id.c_str(), strlen(id.c_str())}																// идентификатор
	};

	CK_ATTRIBUTE pubKeyAttribs[] = 
	{
		{CKA_TOKEN, (CK_VOID_PTR) &bToken, sizeof(bToken)},											// в токене 
		{CKA_LABEL, (CK_VOID_PTR) pubKeyLabel.c_str(), (CK_ULONG) strlen(pubKeyLabel.c_str())},		// метка
		{CKA_GOSTR3410_PARAMS, (CK_VOID_PTR) STR_CRYPTO_PRO_A, sizeof(STR_CRYPTO_PRO_A)},			// тип ключевой пары согласно rfc 4357
		{CKA_GOSTR3411_PARAMS, (CK_VOID_PTR) STR_CRYPTO_PRO_GOST3411, sizeof(STR_CRYPTO_PRO_GOST3411) }, // стандарт алгоритма хэширования
		{CKA_PRIVATE, &bFalse, sizeof(bFalse) },													// виден без ввода пин кода пользователя 
		{CKA_ID, (CK_VOID_PTR) id.c_str(), strlen(id.c_str())}																// идентификатор
	};
	CK_OBJECT_HANDLE hPublicKey, hPrivateKey; // дескрипторы создаваемых ключей

    auto rv = _p11->functions()->C_GenerateKeyPair(
		_session, 
		&mech, 
		&pubKeyAttribs[0],  sizeof(pubKeyAttribs) / sizeof(CK_ATTRIBUTE),
		&privKeyAttribs[0], sizeof(privKeyAttribs) / sizeof(CK_ATTRIBUTE),
		&hPublicKey, 
		&hPrivateKey);
    check_res(rv, "C_GenerateKeyPair");
}

std::vector<CK_UTF8CHAR_PTR> parse_kwargs(const std::vector<std::string>& pairs) {
	std::vector<CK_UTF8CHAR_PTR> ptrVec;
	for(std::size_t i=0; i<pairs.size(); i++) {
		// разобьем строку по '='
		const std::string& s = pairs[i];
		std::size_t pos = s.find('=');
		std::string sname = s.substr(0, pos);
		std::string svalue = s.substr(pos+1);
		
		std::size_t nameLength = strlen(sname.c_str());
		std::size_t valueLength = strlen(svalue.c_str());

		char* name = new char[nameLength+1];
		char* value = new char[valueLength+1];

		std::size_t ncopied;
		ncopied = sname.copy(name, nameLength);
		name[ncopied] = '\0';

		ncopied = svalue.copy(value, valueLength);
		value[ncopied] = '\0';

		ptrVec.push_back((CK_UTF8CHAR_PTR)name);
		ptrVec.push_back((CK_UTF8CHAR_PTR)value);
	}
	return ptrVec;
}

void check_res(CK_RV rv, const char *message) {
	if (rv != CKR_OK) {
		auto msg = P11Utils::GetErrorMessage(message, rv);
		throw std::runtime_error(msg);
	}
}
