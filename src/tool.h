#ifndef TOOL_H

#include <vector>
#include <string>
#include <memory>
#include <exception>

#include "P11Loader.h"

using std::string;
using std::vector;

class JacartaTool {
public:
    JacartaTool(const char* module);
    ~JacartaTool();

    void initialize();
    CK_SLOT_ID findGOSTSlot();
    void openSession(CK_SLOT_ID);
    void initToken(CK_SLOT_ID slot, const string& pin);
    void initPIN(const string& pin);
    void login(const string& pin, bool admin=false);
    void logout();
    void closeSession();
    vector<CK_OBJECT_HANDLE> findCertificates();
    vector<CK_OBJECT_HANDLE> findPublicKeys();
    vector<CK_OBJECT_HANDLE> findPrivateKeys();
    string getCertificateInfo(CK_OBJECT_HANDLE);
    string getKeyInfo(CK_OBJECT_HANDLE);
    void deleteCertificate(CK_OBJECT_HANDLE);
    void createCSR(const string& outputFile,
                   CK_OBJECT_HANDLE publicKey,
                   const vector<string>& dns,
                   const vector<string>& attrs,
                   const vector<string>& exts);
    void storeCertificate(const string& label,
                          const string& file);
    void generateGOSTR3410KeyPair(const string& id,
                                  const string& label);
private:
    vector<CK_OBJECT_HANDLE> findKeys(bool pub=true);

    std::unique_ptr<P11Loader> _p11;
    CK_SESSION_HANDLE _session;
    CK_ULONG _nslots;
};


#endif // !TOOL_H
