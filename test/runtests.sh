#!/bin/sh

./test_init_token.sh && \
./test_create_keys.sh && \
./test_create_csr.sh && \
./test_store_certificate.sh && \
./test_list_certs.sh && \
./test_delete_cert.sh

if [ $? -ne 0 ]; then
    echo -e "There were errors.\n"
else
    echo -e "All tests passed.\n"
fi
