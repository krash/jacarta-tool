#!/bin/sh

echo "Test create CSR..."

rm -f /tmp/csr.der

./jacarta-tool --create-csr -o /tmp/csr.der \
                --login \
                --pin 1234567890 \
                --module /usr/lib64/libjcpkcs11-2.so \
                --dn "street=Baker street 221b" \
                --dn "CN=Sherlock Holmes" \
                --dn "C=RU" \
                --dn "emailAddress=holmes@mailbox.uk" \
                --ext "keyUsage=digitalSignature,nonRepudiation,keyEncipherment,dataEncipherment,keyAgreement" \
                --ext "extendedKeyUsage=critical,clientAuth,emailProtection,1.2.643.2.2.34.6,1.2.643.3.7.0.1.12,1.2.643.3.7.8.1" \
                --ext "1.2.643.100.111=ASN1:UTF8String:jacarta_ecp" || exit 1

if [! ls /tmp/csr.der 1> /dev/null 2>&1 ]; then
    echo "No csr file found"
    exit 1
fi

rm -f /tmp/csr.der

exit 0
