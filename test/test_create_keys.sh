#!/bin/sh

echo "Generating key pair..."
./jacarta-tool --create-keys --id 1 --label "mk1" --login --pin 1234567890 || exit 1

echo "Test list public keys..."
./jacarta-tool --list-public-keys | grep -q mk1 || exit 1

echo "Test list private keys..."
./jacarta-tool --list-private-keys --login --pin 1234567890 | grep -q "mk1 private key" || exit 1
