#!/bin/sh

echo "Test store certificate..."
./jacarta-tool --store-certificate \
               --label "test certificate" \
               --login --pin 1234567890 \
               -f usercert.cer || exit 1
